from fpdf import FPDF

from PIL import Image, ImageFont, ImageDraw
from introduction import writeIntroduction


class Trj_Lir(FPDF):
    def __init__(self, format="A4", orientation="L"):
        super().__init__(format=format, orientation=orientation)
        self.blue = [76.0, 32.0, 250.0]
        self.black = [0.0, 0.0, 0.0]
        self.add_font(
            "firacode", "", "../asset_data/font/firacode/FiraCode-Bold.ttf", uni=True
        )
        self.document_font_firacode = "firacode"


# pdf = FPDF(format="A4",orientation = 'L')
pdf = Trj_Lir(format="A4", orientation="P")
pdf.add_page()
pdf = writeIntroduction(pdf)
pdf.add_page()
pdf.output("C:/Workspace/rpg_jap/Trj_Lir.pdf", "F")
