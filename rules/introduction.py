from fpdf import FPDF


class Introduction:
    def __init__(self, pdf):
        self.pdf: fpdf = pdf

    def firstChapter(self):
        self.pdf.set_xy(40.0, 55.0)
        self.pdf.set_text_color(self.pdf.black[0], self.pdf.black[1], self.pdf.black[2])
        self.pdf.set_font(self.pdf.document_font_firacode, "", 18)
        self.pdf.multi_cell(
            0,
            10,
            "Toto",
        )


def writeIntroduction(pdf: FPDF):
    introduction = Introduction(pdf)
    introduction.firstChapter()
    return introduction.pdf
