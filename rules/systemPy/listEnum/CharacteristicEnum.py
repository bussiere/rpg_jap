import enum


class CharacEnum(enum.Enum):
    STR = "Strenght"
    DEX = "Dexterity"
    CON = "Consitution"
    INT = "Intelligence"
