# -*- coding: utf-8 -*-
"""logp.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/14jN2v9J6tv16nhsntjtxAi4jJM6lOXoW
"""

# Import math Library
import math
from pprint import pprint
import numpy as np

# Return the natural logarithm of different numbers
print(math.log(2.7183))
print(math.log(2))
print(math.log(1.01, 10))

result = []
result_ter = []
result_quater = []
i = 0
# utiliser modulo 10
j = 1
k = 1
l = 1
n = 1
while i < 120:
    if j % 41 == 0:
        result_ter.append(result_quater)
        result_quater = []
        j = 1
        l += 1
        n = 1
    a = (1.01) ** i
    data = {"x": n, "y": l, "a": a, "log(a)": math.log(a, 10)}
    result.append(data)
    result_quater.append(math.log(a, 10))
    n += 1
    m = 1

    i += 1
    j += 1
result_ter.append(result_quater)

pprint(result_ter)

# print(result_bis)

import pandas as pd

print(len(result_quater))

df = pd.DataFrame(result_ter)
df.columns += 3
df.index += 1
# df = df.transpose()
print(df)
print(df.pct_change())
print(df.pct_change(axis="columns"))
