package aurmech.ship;
import java.util.ArrayList;
import java.util.List;
import aurmech.pilot.Pilot;
import aurmech.pawn.Pawn;
import aurmech.listenum.PawnTypeEnum;

public class Ship extends Pawn {

		public List<Pilot> pilots = new ArrayList<Pilot>();

		public Ship(String name) {
			super(name);
			this.pawnTypeEnum = PawnTypeEnum.SHIP;
		}

}