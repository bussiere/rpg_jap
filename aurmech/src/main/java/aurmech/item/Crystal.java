package aurmech.item;
import aurmech.listenum.DurabilityEnum;
import aurmech.listenum.ItemTypeEnum;
import aurmech.listenum.CrystalTypeEnum;
public class Crystal extends Item {
    public DurabilityEnum durabilityEnum;
    public CrystalTypeEnum crystalTypeEnum;
    public Crystal() {
        super();
        this.name = "Crystal";
        this.description = "A crystal that can be used to summon a rosewind.";
        this.type = ItemTypeEnum.CRYSTAL;
        this.crystalTypeEnum = CrystalTypeEnum.NEUTRAL;
    }
}
    