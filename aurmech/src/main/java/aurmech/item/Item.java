package aurmech.item;
import aurmech.listenum.ItemTypeEnum;
import aurmech.dm.utils.GenerateUuid;
public class Item {

	public String name;
	public String description;
	public ItemTypeEnum type;
	public String uuid;

	public Item(String name, String description, ItemTypeEnum type) {
		this.name = name;
		this.description = description;
		this.type = type;
		this.uuid = GenerateUuid.generateUuid();
	}
	public Item() {
		this.uuid = GenerateUuid.generateUuid();
	}


		

}