package aurmech.item;
import aurmech.item.Item;
import aurmech.listenum.WeaponStatusEnum;
import aurmech.listenum.WeaponSlotEnum;
import aurmech.listenum.DamageTypeEnum;
import java.util.List;
import java.util.ArrayList;
public class Weapon extends Item {


    // roll a dice on a 1 roll to dice behind

	public class WeaponRangeCoordinate {
    	public int plusPosX;
    	public int plusPosY;
    	public int plusPosZ;

    	public void initialize(int x,int y,int z){
    		this.plusPosX = x;
        	this.plusPosY = y;
        	this.plusPosZ = z;

    	}
        public  WeaponRangeCoordinate(int x,int y,int z) {
        	initialize(x,y,z);

        }
        public  WeaponRangeCoordinate(int x,int y) {
        	initialize(x,y,0);

        }

    }

	public WeaponStatusEnum weaponStatusEnum;
	public WeaponSlotEnum weaponSlotEnum;
	public DamageTypeEnum damageTypeEnum;

	public float rangeMin;
	public float rangeMax;
	public List<WeaponRangeCoordinate> weaponsRangeCoordinate;
	public float bonusAttack;
	public float dammageMin;
	public float dammageMax;
	public float dammageCrit;
	public float dammageBoost;
	public float[] dammageRange = new float[32];

	public void WeaponInit() {
		float baseDammage = ((this.dammageMax-this.dammageMin)/16.0f);
		this.dammageBoost = baseDammage*14;
        generateDamageRange(baseDammage);
		this.dammageCrit = (this.dammageMax + this.dammageBoost)*1.5f;

	}
    public void generateDamageRange(float baseDammage) {
    	this.dammageRange[0] = this.dammageMin;
    	float baseDammageBoost = this.dammageBoost/15;
    	for (int i =1;i <32;i++) {
    	if (i <= 16){
			this.dammageRange[i] = this.dammageRange[i-1]  + baseDammage ;
		} else {
			this.dammageRange[i] = this.dammageRange[i-1] + baseDammageBoost;
		}
		}
    }

	public void printDammageRange(){
		for (int i =0;i <this.dammageRange.length;i++) {
			System.out.println(i);
			System.out.println(this.dammageRange[i]);
		}
	}
	
}