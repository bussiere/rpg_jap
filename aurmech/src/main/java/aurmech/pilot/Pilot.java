package aurmech.pilot;

import aurmech.listenum.PawnTypeEnum;
import aurmech.pawn.Pawn;
import aurmech.dm.utils.GenerateUuid;
import aurmech.escalation.EscalationDice;

public class Pilot extends Pawn {
	


	public String uuid;
	public float Strenght;
	public float Dexterity;
	public float Constitution;
	public float Intelligence;
	public float Will;
	public float Charisma;
	public float Perception;
	public float Speed;
	public float Luck;
	public float Moral;


	public float StrenghtCurrent;
	public float DexterityCurrent;
	public float ConstitutionCurrent;
	public float IntelligenceCurrent;
	public float WillCurrent;
	public float CharismaCurrent;
	public float PerceptionCurrent;
	public float SpeedCurrent;
	public float LuckCurrent;
	public float MoralCurrent;


    public EscalationDice escalationDice;

    public void init(){
    	this.pawnTypeEnum = PawnTypeEnum.PILOT;
		this.uuid = GenerateUuid.generateUuid();
		this.resetStat();
    }

	public Pilot(String name) {
		super(name);
		init();
	}
	public Pilot() {
		super();
		init();

	}

	public void resetStat() {
		this.StrenghtCurrent = this.Strenght;

	}


	public void escalate(){
		resetStat();
		escalationDice.escalate();
		float bonus = escalationDice.getBonus();
		this.StrenghtCurrent+= bonus;
		this.DexterityCurrent+= bonus;
		this.ConstitutionCurrent+= bonus;
		this.IntelligenceCurrent+= bonus;
		this.WillCurrent+= bonus;
		this.CharismaCurrent+= bonus;
		this.PerceptionCurrent+= bonus;
		this.SpeedCurrent+= bonus;
		this.LuckCurrent+= bonus;
		this.MoralCurrent+= bonus;


	}

	public void printPilot(){
		System.out.println("====================");
		System.out.println("pilot name: " + this.name);
		System.out.println("pilot uuid: " + this.uuid);
		System.out.println("pilot strenght: " + this.Strenght);
		System.out.println("pilot dexterity: " + this.Dexterity);
		System.out.println("pilot constitution: " + this.Constitution);
		System.out.println("pilot intelligence: " + this.Intelligence);
		System.out.println("pilot will: " + this.Will);
		System.out.println("pilot charisma: " + this.Charisma);
		System.out.println("pilot perception: " + this.Perception);
		System.out.println("pilot speed: " + this.Speed);
		System.out.println("pilot luck: " + this.Luck);
		System.out.println("pilot moral: " + this.Moral);
		System.out.println("====================");


	}

}