package aurmech.escalation;

import aurmech.listenum.EscalationDiceEnum;

public class EscalationDice {
	
	public EscalationDiceEnum escalationDiceEnum;
	public float bonusEscalation;
	
	public EscalationDice(){
		this.escalationDiceEnum = EscalationDiceEnum.ESC_0;
		this.bonusEscalation = 0;
	}

	public float getBonus(){
		return this.bonusEscalation;
	}
	
	public void escalate() {
		switch (this.escalationDiceEnum) {
			case ESC_0:
			this.escalationDiceEnum = EscalationDiceEnum.ESC_1;
			this.bonusEscalation = 1;
			break;
			case ESC_1:
			this.escalationDiceEnum = EscalationDiceEnum.ESC_2;
			this.bonusEscalation = 2;
			break;
			case ESC_2:
			this.escalationDiceEnum = EscalationDiceEnum.ESC_3;
			this.bonusEscalation = 3;
			break;
			case ESC_3:
			this.escalationDiceEnum = EscalationDiceEnum.ESC_4;
			this.bonusEscalation = 4;
			break;
			case ESC_4:
			this.escalationDiceEnum = EscalationDiceEnum.ESC_5;
			this.bonusEscalation = 5;
			break;
			case ESC_5:
			this.escalationDiceEnum = EscalationDiceEnum.ESC_6;
			this.bonusEscalation = 6;
			break;
			case ESC_6:
			this.escalationDiceEnum = EscalationDiceEnum.ESC_6;
			this.bonusEscalation = 6;
			break;
		}
	}
	
}