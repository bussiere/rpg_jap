package aurmech.map;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap; 
import aurmech.map.Hex;
public class MapMaster {


    HashMap<String, Hex> hexesByUuid = new HashMap<String, Hex>();
    HashMap<Integer, Hex> hexesByPosition = new HashMap<Integer, Hex>();
    

    public void addHexByPositionAndUuid(int x,int y,Hex hex) {
        hexesByPosition.put(x*1000 + y,hex);
        addHexByUuid(hex.uuid,hex);
    }

    public void updateHexToMap(Hex hex) {
        hexesByPosition.put(hex.posX*1000 + hex.posY,hex);
        hexesByUuid.put(hex.uuid,hex);
    }

    public void addHexByPosition(int x,int y,Hex hex) {
        hexesByPosition.put(x*1000 + y, hex);
    }
    public Hex getHexByPosition(int x,int y) {
        return hexesByPosition.get(x*1000 + y);
    }

    public void addHexByUuid(String uuid, Hex hex) {
        hexesByUuid.put(hex.uuid,hex);
    }


	
}