package aurmech.listenum;

public enum WeaponSlotEnum {
    RIGHT_HAND,
    LEFT_HAND,
    TWO_HAND,
    RIGHT_OR_LEFT_HAND,
    BACK,
}
