package aurmech.dm.combat;
import aurmech.listenum.OrientationEnum;
import aurmech.listenum.DamageTypeEnum;
import aurmech.dm.utils.GenerateUuid;
import aurmech.item.Weapon;
import aurmech.item.Weapon.WeaponRangeCoordinate;
import aurmech.listenum.CrystalTypeEnum;
/*
 * 


 * 
*/

public class Attack {

		public String uuid;
		public OrientationEnum orientationOrigin;
		public String attackerSquadUuid;
		public String attackerUuid;
		public String hexagonOriginUuid;
		public String weaponUuid;
		public String factionUuid;
		public DamageTypeEnum damageTypeEnum;
		public CrystalTypeEnum crystalTypeEnum;
		public int angleAttackFromOrigin;
		public int angleAttackDefender;
		public WeaponRangeCoordinate weaponRangeCoordinate;
		public Weapon weapon;

		public Attack(){
			this.uuid = GenerateUuid.generateUuid();
		}


}