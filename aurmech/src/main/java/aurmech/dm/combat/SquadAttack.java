package aurmech.dm.combat;

import aurmech.map.Range;
import aurmech.map.Hex;
import aurmech.item.Weapon;
import aurmech.item.Weapon.WeaponRangeCoordinate;
import aurmech.mech.Mech;
import aurmech.squad.Squad;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap; 
import aurmech.dm.combat.Attack;
import aurmech.listenum.DamageTypeEnum;
import aurmech.listenum.CrystalTypeEnum;
import aurmech.dm.combat.AttackBySquare;

/*
 * 
 * 
public enum CrystalTypeEnum {
    NEUTRAL,
    LIGHT,
    SHADOW,
    FLORA,
    WATER,
    FIRE,
    ICE,
    EARTH,
    VOLT,
    WIND,
    TOXIN,

    
}

 * 
 */
public class SquadAttack {

	public int minRangeAttacks;
	public int maxRangeAttacks;


	public HashMap<Integer, AttackBySquare> listAttackBySquare = new HashMap<Integer, AttackBySquare>();



	public void addAttack(Squad squad,Mech mech,Weapon weapon,int rangeDamage){
		addAttack(squad,mech,weapon,rangeDamage,false);
	}
    // Todo calulate all atacks from a square
	public void addAttack(Squad squad,Mech mech,Weapon weapon,int rangeDamage,boolean critic) {
		for (WeaponRangeCoordinate weaponRangeCoordinate : weapon.weaponsRangeCoordinate){
			AttackBySquare attackBySquare = listAttackBySquare.get((squad.posX+weaponRangeCoordinate.plusPosX)*1000+(squad.posY+weaponRangeCoordinate.plusPosY));
			if (attackBySquare == null){
				attackBySquare = new AttackBySquare();
			}
			if (weapon.damageTypeEnum == DamageTypeEnum.LASER){
				if (mech.crystal.crystalTypeEnum == CrystalTypeEnum.NEUTRAL){
					attackBySquare.neutralLaserDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.LIGHT) {
					attackBySquare.lightLaserDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.SHADOW) {
					attackBySquare.shadowLaserDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.FLORA) {
					attackBySquare.floraLaserDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.WATER) {
					attackBySquare.waterLaserDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.FIRE) {
					attackBySquare.fireLaserDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.ICE) {
					attackBySquare.iceLaserDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.EARTH) {
					attackBySquare.earthLaserDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.VOLT) {
					attackBySquare.voltLaserDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.WIND) {
					attackBySquare.windLaserDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.TOXIN) {
					attackBySquare.toxinLaserDamage += weapon.dammageRange[rangeDamage];
				}
			} else if (weapon.damageTypeEnum == DamageTypeEnum.KINETIC) {
				if (mech.crystal.crystalTypeEnum == CrystalTypeEnum.NEUTRAL){
					attackBySquare.neutralKineticDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.LIGHT){
					attackBySquare.lightKineticDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.SHADOW){
					attackBySquare.shadowKineticDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.FLORA){
					attackBySquare.floraKineticDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.WATER){
					attackBySquare.waterKineticDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.FIRE){
					attackBySquare.fireKineticDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.ICE){
					attackBySquare.iceKineticDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.EARTH){
					attackBySquare.earthKineticDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.VOLT){
					attackBySquare.voltKineticDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.WIND){
					attackBySquare.windKineticDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.TOXIN){
					attackBySquare.toxinKineticDamage += weapon.dammageRange[rangeDamage];
				}
			} else if (weapon.damageTypeEnum == DamageTypeEnum.MISSILE) {
				if (mech.crystal.crystalTypeEnum == CrystalTypeEnum.NEUTRAL){
					attackBySquare.neutralMissileDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.LIGHT){
					attackBySquare.lightMissileDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.SHADOW){
					attackBySquare.shadowMissileDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.FLORA){
					attackBySquare.floraMissileDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.WATER){
					attackBySquare.waterMissileDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.FIRE){
					attackBySquare.fireMissileDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.ICE){
					attackBySquare.iceMissileDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.EARTH){
					attackBySquare.earthMissileDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.VOLT){
					attackBySquare.voltMissileDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.WIND){
					attackBySquare.windMissileDamage += weapon.dammageRange[rangeDamage];
				} else if  (mech.crystal.crystalTypeEnum == CrystalTypeEnum.TOXIN){
					attackBySquare.toxinMissileDamage += weapon.dammageRange[rangeDamage];
				}
			}
			attackBySquare.totalDamage += weapon.dammageRange[rangeDamage];
			Attack attack = new Attack();
			// TODO
			attack.angleAttackFromOrigin = mech.orientationDegree;
			attack.attackerSquadUuid = squad.uuid;
			attack.attackerUuid = mech.uuid;
			attack.damageTypeEnum = weapon.damageTypeEnum;
			attack.hexagonOriginUuid = squad.hexagonUuid;
			attack.weaponRangeCoordinate = weaponRangeCoordinate;
			attack.weapon = weapon;
			attack.crystalTypeEnum = mech.crystal.crystalTypeEnum;
			attackBySquare.listAttack.put(attackBySquare.nbAttack,attack);
			attackBySquare.nbAttack++;
			listAttackBySquare.put((squad.posX+weaponRangeCoordinate.plusPosX)*1000+(squad.posY+weaponRangeCoordinate.plusPosY),attackBySquare);
		}
	}


public void calculateAttackBySquare(Squad squad){
	calculateAttackBySquare(squad,10);
}
	public void calculateAttackBySquare(Squad squad,int rangeDamage){


		for (Mech mech : squad.mechs) {
			List<Weapon> weapons = new ArrayList<Weapon>();
			weapons.add(mech.rightHandWeapon);
			weapons.add(mech.leftHandWeapon);
			weapons.add(mech.backWeapon);
			for(Weapon weapon : weapons){
				if (weapon != null){
					addAttack(squad,mech,weapon,rangeDamage);
				}
			}
		}     
		

	}
}