package aurmech.dm.utils;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang3.RandomStringUtils;
import java.time.Instant;
public class GenerateUuid {

	public static String generateUuid(){

		Long millis = Instant.now().toEpochMilli();
        return Long.toHexString(millis)+RandomStringUtils.randomAlphanumeric(8);


	}
}