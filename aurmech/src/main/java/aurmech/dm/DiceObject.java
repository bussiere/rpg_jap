package aurmech.dm;

import aurmech.dm.utils.GenerateUuid;
import aurmech.listenum.DiceTypeEnum;
public class DiceObject {

	public int value;
	public int nbFace;
	public String uuid;
	public DiceTypeEnum diceType;

	public DiceObject(){
		this.uuid = GenerateUuid.generateUuid();
	}



}