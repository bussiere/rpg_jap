package aurmech.exception;

public class PositionNotFoundException extends Exception  {
    public PositionNotFoundException(String errorMessage) {  
        super(errorMessage);  
    }
    
}
