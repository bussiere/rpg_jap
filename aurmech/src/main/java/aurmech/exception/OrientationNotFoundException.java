package aurmech.exception;

public class OrientationNotFoundException extends Exception  {
    public OrientationNotFoundException(String errorMessage) {  
        super(errorMessage);  
    }
    
}
