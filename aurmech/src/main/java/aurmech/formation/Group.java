package aurmech.formation;

import java.util.ArrayList;
import java.util.List;
import aurmech.pilot.Pilot;
import aurmech.pawn.Pawn;
import aurmech.mech.Mech;
import aurmech.ship.Ship;
import aurmech.dm.utils.GenerateUuid;
public class Group {

    public List<Mech> mechs;
    public List<Ship> ships;
    public List<Pilot> pilots;
    public List<Pawn> pawns;
    public String uuid;

    public Group() {
        this.mechs = new ArrayList<Mech>();
        this.ships = new ArrayList<Ship>();
        this.pilots = new ArrayList<Pilot>();
        this.pawns = new ArrayList<Pawn>();
        this.uuid = GenerateUuid.generateUuid();
    }

    public List<Pawn> generateListPawns() {
        this.pawns = new ArrayList<Pawn>();
        for (Mech mech : mechs) {
            this.pawns.add((Pawn)mech);
        }
        for (Ship ship : ships) {
            this.pawns.add((Pawn)ship);
        }
        for (Pilot pilot : pilots) {
            this.pawns.add((Pawn)pilot);
        }
        return this.pawns;
    }

    public void addUnit(Mech mech){
        this.mechs.add(mech);
    }
    public void addUnit(Ship ship){
        this.ships.add(ship);
    }
    public void addUnit(Pilot pilot){
        this.pilots.add(pilot);
    }
    public void addUnit(Pawn pawn){
        if (pawn instanceof Mech) {
            this.mechs.add((Mech)pawn);
        } else if (pawn instanceof Ship) {
            this.ships.add((Ship)pawn);
        } else if (pawn instanceof Pilot) {
            this.pilots.add((Pilot)pawn);
        }
    }
    public void removeUnit(Mech mech){
        this.mechs.remove(mech);
    }
    public void removeUnit(Ship ship){
        this.ships.remove(ship);
    }
    public void removeUnit(Pilot pilot){
        this.pilots.remove(pilot);
    }
    public void removeUnit(Pawn pawn){
        if (pawn instanceof Mech) {
            this.mechs.remove((Mech)pawn);
        } else if (pawn instanceof Ship) {
            this.ships.remove((Ship)pawn);
        } else if (pawn instanceof Pilot) {
            this.pilots.remove((Pilot)pawn);
        }
        this.pawns.remove(pawn);
    }

    // Remove unit from List<Pawn> pawns
    public void removePawn(String uuid){
        for (Pawn pawn : pawns) {
            if (pawn.uuid.equals(uuid)) {
                this.pawns.remove(pawn);
                removeUnitSpe(uuid);
                return;
            }
        }
        
    }
    public void removeUnitSpe(String uuid){
        for (Mech mech : mechs) {
            if (mech.uuid.equals(uuid)) {
                this.mechs.remove(mech);
                return;
            }
        }
        for (Ship ship : ships) {
            if (ship.uuid.equals(uuid)) {
                this.ships.remove(ship);
                return;
            }
        }
        for (Pilot pilot : pilots) {
            if (pilot.uuid.equals(uuid)) {
                this.pilots.remove(pilot);
                return;
            }
        }
    }

    public Pawn getPawn(String uuid) {
        for (Pawn pawn : this.pawns) {
            if (pawn.uuid.equals(uuid)) {
                return pawn;
            }
        }
        return null;
    }
    // TODO use dictionnary for list of mech ,squad , pilot, ship
    
}
