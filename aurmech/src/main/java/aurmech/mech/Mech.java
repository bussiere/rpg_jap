package aurmech.mech;

import aurmech.item.MechPart;
import aurmech.item.Weapon;
import aurmech.item.Crystal;
import aurmech.pilot.Pilot;
import aurmech.pawn.Pawn;
import aurmech.listenum.PawnTypeEnum;
import aurmech.listenum.MechFormEnum;

public class Mech extends Pawn {
	

	public MechPart head;
	public MechPart torso;
	public MechPart rightHand;
	public MechPart leftHand;
	public MechPart rightLeg;
	public MechPart leftLeg;

	public Crystal crystal;




	public Weapon rightHandWeapon;
	public Weapon leftHandWeapon;
	public Weapon backWeapon;


	public float shieldResistanceSlotLaser;
	public float shieldResistanceSlotKinetic;
	public float shieldResistanceSlotMissile;

	public MechFormEnum mechFormEnum;

	public int Energy;


    public int Health;




    
   
    public int PowernessCurrent;
	public int ManiabilityCurrent;
	public int ProcessingCurrent;
	public int VisionCurrent;
	public int MovementCurrent;
	public int ArmorCurrent;
	public int HealthCurrent;
	public int ReactionCurrent;

	// pour le bonus de charism aux unités adjacente
	public int ComLinkCurrent;

	public int EnergyCurrent;


    public int LastTurnFormChange;

	public int Form1Powerness;
	public int Form1Maniability;
	public int Form1Processing;
	public int Form1Movement;
	public int Form1Vision;
	// pour le bonus de charism aux unités adjacente
	public int Form1ComLink;
    public int Form1EnergyConsumption;


	public float Form1MechStrenght;
	public float Form1MechDexterity;
	public float Form1MechIntelligence;
	public float Form1MechWill;
	public float Form1MechCharisma;
	public float Form1MechPerception;
	public float Form1MechSpeed;
	public float Form1MechLuck;


	public int Form2Powerness;
	public int Form2Maniability;
	public int Form2Processing;
	public int Form2Movement;
	// pour le bonus de charism aux unités adjacente
	public int Form2ComLink;
	public int Form2EnergyConsumption;


	public float Form2MechStrenght;
	public float Form2MechDexterity;
	public float Form2MechIntelligence;
	public float Form2MechWill;
	public float Form2MechCharisma;
	public float Form2MechPerception;
	public float Form2MechSpeed;
	public float Form2MechLuck;
	


	public int Form3Powerness;
	public int Form3Maniability;
	public int Form3Processing;
	public int Form3Movement;
	// pour le bonus de charism aux unités adjacente
	public int Form3ComLink;
	public int Form3EnergyConsumption;


	public float Form3MechStrenght;
	public float Form3MechDexterity;
	public float Form3MechIntelligence;
	public float Form3MechWill;
	public float Form3MechCharisma;
	public float Form3MechPerception;
	public float Form3MechSpeed;
	public float Form3MechLuck;


	public Pilot pilot;

	public float MechStrenght;
	public float MechDexterity;
	public float MechIntelligence;
	public float MechWill;
	public float MechCharisma;
	public float MechPerception;
	public float MechSpeed;
	public float MechLuck;


	public float MechStrenghtCurrent;
	public float MechDexterityCurrent;
	public float MechIntelligenceCurrent;
	public float MechWillCurrent;
	public float MechCharismaCurrent;
	public float MechPerceptionCurrent;
	public float MechSpeedCurrent;
	public float MechLuckCurrent;


	public float meleeAttackScoreCurrent ;
	public float rangeAttackScoreCurrent;
	public float evasionScoreCurrent ;
	public float initScoreCurrent ;
	public float criticalScoreCurrent;
	public float presenceScoreCurrent ;

	public void transformMechForm(MechFormEnum mechFormEnum) {
		this.mechFormEnum = mechFormEnum;
		if (mechFormEnum == MechFormEnum.FORM1){
			this.MechStrenghtCurrent += Form1MechStrenght;
			this.MechDexterityCurrent += Form1MechDexterity;
			this.MechIntelligenceCurrent += Form1MechIntelligence;
			this.MechWillCurrent += Form1MechWill;
			this.MechCharismaCurrent += Form1MechCharisma;
			this.MechPerceptionCurrent += Form1MechPerception;
			this.MechSpeedCurrent += Form1MechSpeed;
			this.MechLuckCurrent += Form1MechLuck;
		} else if (mechFormEnum == MechFormEnum.FORM2){
			this.MechStrenghtCurrent += Form2MechStrenght;
			this.MechDexterityCurrent += Form2MechDexterity;
			this.MechIntelligenceCurrent += Form2MechIntelligence;
			this.MechWillCurrent += Form2MechWill;
			this.MechCharismaCurrent += Form2MechCharisma;
			this.MechPerceptionCurrent += Form2MechPerception;
			this.MechSpeedCurrent += Form2MechSpeed;
			this.MechLuckCurrent += Form2MechLuck;
		} else if (mechFormEnum == MechFormEnum.FORM3){
			this.MechStrenghtCurrent += Form3MechStrenght;
			this.MechDexterityCurrent += Form3MechDexterity;
			this.MechIntelligenceCurrent += Form3MechIntelligence;
			this.MechWillCurrent += Form3MechWill;
			this.MechCharismaCurrent += Form3MechCharisma;
			this.MechPerceptionCurrent += Form3MechPerception;
			this.MechSpeedCurrent += Form3MechSpeed;
			this.MechLuckCurrent += Form3MechLuck;

		}
	}

	public Mech(String name) {
		super(name);
		this.pawnTypeEnum = PawnTypeEnum.MECH;
		this.mechFormEnum = MechFormEnum.FORM1;
	}
	public Mech() {
		super();
		this.pawnTypeEnum = PawnTypeEnum.MECH;
		this.mechFormEnum = MechFormEnum.FORM1;
	}


    public void recalcMechStat(){

    	this.MechStrenghtCurrent += (this.pilot.StrenghtCurrent/100)*this.PowernessCurrent;
    	this.MechDexterityCurrent += (this.pilot.DexterityCurrent/100)*this.ManiabilityCurrent;
    	this.MechIntelligenceCurrent += (this.pilot.IntelligenceCurrent/100)*this.ProcessingCurrent;
    	this.MechWillCurrent += (this.pilot.WillCurrent/100)*this.ProcessingCurrent;
    	this.MechSpeedCurrent += (this.pilot.SpeedCurrent/100)*this.ReactionCurrent;
    	this.MechPerceptionCurrent += (this.pilot.PerceptionCurrent/100)*this.VisionCurrent;
    	this.MechCharismaCurrent += (this.pilot.CharismaCurrent/100)*this.ComLinkCurrent;
    }
	public void sitPilot(Pilot pilot) {
		this.pilot = pilot;
		transformMechForm(this.mechFormEnum);
		recalcMechStat();
		recalcStatCombat();
	}
	public void escalatePilot(Pilot pilot) {
		this.pilot = pilot;
		this.pilot.resetStat();
		this.pilot.escalate();
		transformMechForm(this.mechFormEnum);
		recalcMechStat();
		recalcStatCombat();
	}


	public void recalcStatCombat() {

		this.meleeAttackScoreCurrent = ((this.MechStrenght*6+this.MechDexterity*3+this.pilot.Luck)/10);
		this.rangeAttackScoreCurrent = ((this.MechPerception*6+this.MechDexterity*3+this.pilot.Luck)/10);
		this.evasionScoreCurrent = ((this.MechSpeed*6+this.MechIntelligence*3+this.pilot.Luck)/30);
		this.initScoreCurrent = ((this.MechSpeed*6+this.MechWill*3+this.MechIntelligence)/10);
		this.criticalScoreCurrent = ((this.MechWill*3+this.pilot.Luck)/10);
		this.presenceScoreCurrent = ((this.MechCharisma*6+this.MechWill*3)/9);
	}

	public void printMech(){
		System.out.println("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°");
		System.out.println("Mech : " + this.name);
		System.out.println("Pilot : " + this.pilot.name);
		System.out.println("Pilot Stat");
		this.pilot.printPilot();
		System.out.println("Form : " + this.mechFormEnum);
		System.out.println("Powerness : " + this.PowernessCurrent);
		System.out.println("Maniability : " + this.ManiabilityCurrent);
		System.out.println("Processing : " + this.ProcessingCurrent);
		System.out.println("Reaction : " + this.ReactionCurrent);
		System.out.println("Vision : " + this.VisionCurrent);
		System.out.println("ComLink : " + this.ComLinkCurrent);
		System.out.println("Strenght : " + this.MechStrenght);
		System.out.println("Dexterity : " + this.MechDexterity);
		System.out.println("Intelligence : " + this.MechIntelligence);
		System.out.println("Will : " + this.MechWill);
		System.out.println("Charisma : " + this.MechCharisma);
		System.out.println("Perception : " + this.MechPerception);
		System.out.println("Speed : " + this.MechSpeed);
		System.out.println("Luck : " + this.MechLuck);
		System.out.println("Melee Attack Score : " + this.meleeAttackScore);
		System.out.println("Range Attack Score : " + this.rangeAttackScore);
		System.out.println("Evasion Score : " + this.evasionScore);
		System.out.println("Init Score : " + this.initScore);
		System.out.println("Critical Score : " + this.criticalScore);
		System.out.println("Presence Score : " + this.presenceScore);
		System.out.println("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°");

	}
}