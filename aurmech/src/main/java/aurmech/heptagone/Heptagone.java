package aurmech.rosewind;

import aurmech.listenum.HeptagoneTurnEnum;

public class Heptagone {

	public int turn;
	public HeptagoneTurnEnum roseWindTurnEnum;

	public class RosewindToken {
		public int roseToken;
		public HeptagoneTurnEnum roseWindTurnEnum;
	}

	public class RosewindTurn {
		public int roseToken;
		public HeptagoneTurnEnum roseWindTurnEnum;
	}


}