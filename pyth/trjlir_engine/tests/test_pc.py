from trjlir_engine.pcs.pc_basic import PcBasic
from trjlir_engine.classes.class_basic import ClassBasic


def test_pc():
    john_doe = PcBasic(name="john_doe",might=17,constitution=17)
    first_class = ClassBasic(name="titi")
    second_class = ClassBasic(name="titi")
    john_doe.Calc_first_class(first_class,second_class)
    john_doe.Calc_second_class(second_class,first_class)
    print("john_doe.willpower")
    print(john_doe.willpower)
    print("john_doe.fortitude")
    print(john_doe.fortitude)
