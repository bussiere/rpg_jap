from trjlir_engine.rules.element_enum import ElementEnum
from trjlir_engine.rules.stat_enum import StatEnum


class ClassBasic:
    def __init__(
        self,
        name="",
        level=1,
    ):
        self.name = name
        self.level = level
        self.willpower_stat_used = StatEnum.MIG
        self.fortitude_stat_used = StatEnum.CON
        self.bonus_willpower = 0
        self.bonus_fortitude = 0

    def __str__(self):
        return self.name
