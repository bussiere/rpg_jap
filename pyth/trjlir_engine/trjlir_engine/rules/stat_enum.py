import enum


class StatEnum(enum.Enum):
    STR = "strenght"
    DEX = "dexterity"
    CON = "constitution"
    INT = "intelligence"
    MIG = "might"
    CHA = "charisma"
