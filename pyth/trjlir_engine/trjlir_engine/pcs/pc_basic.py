from trjlir_engine.rules import stats_rules
from math import floor
from trjlir_engine.classes.class_basic import ClassBasic
from trjlir_engine.rules.element_enum import ElementEnum
from trjlir_engine.rules.stat_enum import StatEnum
from trjlir_engine.rules.stats_rules import Stats

class PcBasic:
    def __init__(
        self,
        name="",
        mind: int = 0,
        body: int = 0,
        skill: int = 0,
        strength: int = 0,
        dexterity: int = 0,
        constitution=0,
        intelligence=0,
        might=0,
        charisma=0,
        classes=[],
        first_class=None,
        second_class=None,
        fortitude=0,
        willpower=0,
        bloodied=0,
        nb_strike=0,
    ):
        self.mind = name
        self.body = body
        self.skill = skill
        self.strength = strength
        self.dexterity = dexterity
        self.constitution = constitution
        self.intelligence = intelligence
        self.might = might
        self.charisma = charisma
        self.classes = classes
        self.fortitude = fortitude
        self.willpower = willpower
        self.first_class = first_class
        if self.first_class != None:
            self.first_class_level = self.first_class.level
        else:
            self.first_class_level = 0
        self.second_class = second_class
        if self.second_class != None:
            self.second_class_level = self.second_class.level
        else:
            self.second_class_level = 0
        self.bloodied = bloodied
        self.base_evasion = 0
        self.base_defense = 0
        self.AC = 0
        self.hp = 0
        self.mp = 0
        self.nb_strike = (nb_strike,)

    def calc_bloodied(self):
        self.bloodied = floor(self.hp / 2)

    def calc_fortitude(self,stats=Stats()):
        if self.first_class.fortitude_stat_used == StatEnum.STR:
            self.fortitude_stat = self.strength
        elif self.first_class.fortitude_stat_used == StatEnum.CON:
            self.fortitude_stat = self.constitution
        elif self.first_class.fortitude_stat_used == StatEnum.DEX:
            self.fortitude_stat = self.dexterity
        elif self.first_class.fortitude_stat_used == StatEnum.INT:
            self.fortitude_stat = self.intelligence
        elif self.first_class.fortitude_stat_used == StatEnum.MIG:
            self.fortitude_stat = self.might
        elif self.first_class.fortitude_stat_used == StatEnum.CHA:
            self.fortitude_stat = self.charisma
        self.fortitude_stat_second = 0
        if self.second_class is not None:
            if self.second_class.fortitude_stat_used == StatEnum.STR:
                self.fortitude_stat_second = self.strength
            elif self.second_class.fortitude_stat_used == StatEnum.CON:
                self.fortitude_stat_second = self.constitution
            elif self.second_class.fortitude_stat_used == StatEnum.DEX:
                self.fortitude_stat_second = self.dexterity
            elif self.second_class.fortitude_stat_used == StatEnum.INT:
                self.fortitude_stat_second = self.intelligence
            elif self.second_class.fortitude_stat_used == StatEnum.MIG:
                self.fortitude_stat_second = self.might
            elif self.second_class.fortitude_stat_used == StatEnum.CHA:
                self.fortitude_stat_second = self.charisma
        self.fortitude = (
            stats.bonus_stat[self.fortitude_stat]
            + self.first_class.bonus_fortitude
            + floor(stats.bonus_stat[self.fortitude_stat_second] / 2)
            + floor(self.second_class.bonus_fortitude / 2)
            + self.first_class_level
            + floor(self.second_class_level / 2)
        )

    def calc_willpower(self,stats=Stats()):
        if self.first_class.willpower_stat_used == StatEnum.STR:
            self.willpower_stat = self.strength
        elif self.first_class.willpower_stat_used == StatEnum.CON:
            self.willpower_stat = self.constitution
        elif self.first_class.willpower_stat_used == StatEnum.DEX:
            self.willpower_stat = self.dexterity
        elif self.first_class.willpower_stat_used == StatEnum.INT:
            self.willpower_stat = self.intelligence
        elif self.first_class.willpower_stat_used == StatEnum.MIG:
            self.willpower_stat = self.might
        elif self.first_class.willpower_stat_used == StatEnum.CHA:
            self.willpower_stat = self.charisma
        self.willpower_stat_second = 0
        if self.second_class is not None:
            if self.second_class.willpower_stat_used == StatEnum.STR:
                self.willpower_stat_second = self.strength
            elif self.second_class.willpower_stat_used == StatEnum.CON:
                self.willpower_stat_second = self.constitution
            elif self.second_class.willpower_stat_used == StatEnum.DEX:
                self.willpower_stat_second = self.dexterity
            elif self.second_class.willpower_stat_used == StatEnum.INT:
                self.willpower_stat_second = self.intelligence
            elif self.second_class.willpower_stat_used == StatEnum.MIG:
                self.willpower_stat_second = self.might
            elif self.second_class.willpower_stat_used == StatEnum.CHA:
                self.willpower_stat_second = self.charisma
        self.willpower = (
            stats.bonus_stat[self.willpower_stat]
            + self.first_class.bonus_willpower
            + floor(stats.bonus_stat[self.willpower_stat_second] / 2)
            + floor(self.second_class.bonus_willpower / 2)
            + self.first_class_level
            + floor(self.second_class_level / 2)
        )

    def Calc_first_class(self, first_class=None,second_class=None):
        if first_class != None:
            self.first_class = first_class
        if second_class != None:
            self.second_class = second_class
        self.first_class_level = self.first_class.level
        self.calc_willpower()
        self.calc_fortitude()

    def Calc_second_class(self, second_class=None, first_class=None):
        if second_class != None:
            self.second_class = second_class
        if first_class != None:
            self.first_class = first_class
        self.second_class_level = self.second_class.level
        self.calc_willpower()
        self.calc_fortitude()
