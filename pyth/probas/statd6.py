import json

""" 
Roll	Freq	Prob	
3	1	0.00463	
4	3	0.01389	
5	6	0.02778	
6	10	0.04630	
7	15	0.06944	
8	21	0.09722	
9	25	0.11574	
10	27	0.12500	
11	27	0.12500	
12	25	0.11574	
13	21	0.09722	
14	15	0.06944	
15	10	0.04630	
16	6	0.02778	
17	3	0.01389	
18	1	0.00463"""

dice_probability = {
    3: 0.00463,
    4: 0.01389,
    5: 0.02778,
    6: 0.04630,
    7: 0.06944,
    8: 0.09722,
    9: 0.11574,
    10: 0.12500,
    11: 0.12500,
    12: 0.11574,
    13: 0.09722,
    14: 0.06944,
    15: 0.04630,
    16: 0.02778,
    17: 0.01389,
    18: 0.00463,
}
dice_frequency = {
    3: 1,
    4: 3,
    5: 6,
    6: 10,
    7: 15,
    8: 21,
    9: 25,
    10: 27,
    11: 27,
    12: 25,
    13: 21,
    14: 15,
    15: 10,
    16: 6,
    17: 3,
    18: 1,
}

result = {}
total_frequency = sum(dice_frequency.values())
one_percent = total_frequency / 100
total_before_frequency = 0
total_before_percent = 0
for i in range(3, 19):
    result[i] = {}
    result[i]["frequency"] = dice_frequency[i]
    result[i]["probability"] = dice_probability[i] * 100
    result[i]["cumulative"] = total_before_frequency + dice_frequency[i]
    result[i]["cumulative_percent"] = (total_before_percent + dice_probability[i]) * 100
    total_before_frequency += dice_frequency[i]
    total_before_percent += dice_probability[i]
with open("diced6.json", "w") as f:
    json.dump(result, f)
