import json

"""
2d12 Table
Roll	Freq	Prob	
2	1	0.00694	
3	2	0.01389	
4	3	0.02083	
5	4	0.02778	
6	5	0.03472	
7	6	0.04167	
8	7	0.04861	
9	8	0.05556	
10	9	0.06250	
11	10	0.06944	
12	11	0.07639	
13	12	0.08333	
14	11	0.07639	
15	10	0.06944	
16	9	0.06250	
17	8	0.05556	
18	7	0.04861	
19	6	0.04167	
20	5	0.03472	
21	4	0.02778	
22	3	0.02083	
23	2	0.01389	
24	1	0.00694
"""

""" 
3d12 Table
Roll	Freq	Prob	
3	1	0.00058	
4	3	0.00174	
5	6	0.00347	
6	10	0.00579	
7	15	0.00868	
8	21	0.01215	
9	28	0.01620	
10	36	0.02083	
11	45	0.02604	
12	55	0.03183	
13	66	0.03819	
14	78	0.04514	
15	88	0.05093	
16	96	0.05556	
17	102	0.05903	
18	106	0.06134	
19	108	0.06250	
20	108	0.06250	
21	106	0.06134	
22	102	0.05903	
23	96	0.05556	
24	88	0.05093	
25	78	0.04514	
26	66	0.03819	
27	55	0.03183	
28	45	0.02604	
29	36	0.02083	
30	28	0.01620	
31	21	0.01215	
32	15	0.00868	
33	10	0.00579	
34	6	0.00347	
35	3	0.00174	
36	1	0.00058 """

dice_probability = {
    3: 0.00058,
    4: 0.00174,
    5: 0.00347,
    6: 0.00579,
    7: 0.00868,
    8: 0.01215,
    9: 0.01620,
    10: 0.02083,
    11: 0.02604,
    12: 0.03183,
    13: 0.03819,
    14: 0.04514,
    15: 0.05093,
    16: 0.05556,
    17: 0.05903,
    18: 0.06134,
    19: 0.06250,
    20: 0.06250,
    21: 0.06134,
    22: 0.05903,
    23: 0.05556,
    24: 0.05093,
    25: 0.04514,
    26: 0.03819,
    27: 0.03183,
    28: 0.02604,
    29: 0.02083,
    30: 0.01620,
    31: 0.01215,
    32: 0.00868,
    33: 0.00579,
    34: 0.00347,
    35: 0.00174,
    36: 0.00058,
}
dice_frequency = {
    3: 1,
    4: 3,
    5: 6,
    6: 10,
    7: 15,
    8: 21,
    9: 28,
    10: 36,
    11: 45,
    12: 55,
    13: 66,
    14: 78,
    15: 88,
    16: 96,
    17: 102,
    18: 106,
    19: 108,
    20: 108,
    21: 106,
    22: 102,
    23: 96,
    24: 88,
    25: 78,
    26: 66,
    27: 55,
    28: 45,
    29: 36,
    30: 28,
    31: 21,
    32: 15,
    33: 10,
    34: 6,
    35: 3,
    36: 1,
}
dice12 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
result = {}
total_frequency = sum(dice_frequency.values())
one_percent_all = float(total_frequency) / 100
total_before_frequency = 0
total_before_percent = 0
for i in range(3, 37):
    result[i] = {}
    result[i]["frequency"] = dice_frequency[i]
    result[i]["probability"] = dice_probability[i] * 100
    result[i]["cumulative"] = total_before_frequency + dice_frequency[i]
    result[i]["cumulative_percent"] = (total_before_percent + dice_probability[i]) * 100
    total_before_frequency += dice_frequency[i]
    total_before_percent += dice_probability[i]
with open("diced12.json", "w") as f:
    json.dump(result, f)

result_double = {}
result_triple = {}
total_double = 0
total_triple = 0
combo_triple = []
combo_double = []
for d1 in dice12:
    for d2 in dice12:
        for d3 in dice12:
            if d1 == d2 == d3:
                if [d1, d2, d3] not in combo_triple:
                    combo_triple.append([d1, d2, d3])
                    result_triple.setdefault(d1, 0)
                    result_triple[d1] += 1
                    total_triple += 1
            if d1 == d2 or d2 == d3 or d1 == d3:
                if [d1, d2, d3] not in combo_double:
                    combo_double.append([d1, d2, d3])
                    result_double.setdefault(d1, 0)
                    result_double[d1] += 1
                    total_double += 1

one_percent_double = float(total_double) / 100
one_percent_triple = float(total_triple) / 100

result_total = {}
result_total["double"] = {}
result_total["triple"] = {}
total_cumulative_double = 0
total_cumulative_probability_double = 0
total_cumulative_triple = 0
total_cumulative_probability_triple = 0
for key in result_double:
    result_total["double"][key] = {}
    result_total["double"][key]["frequency"] = result_double[key]
    result_total["double"][key]["probability"] = float(result_double[key]) / float(
        one_percent_all
    )
    result_total["double"][key]["cumulative"] = (
        result_double[key] + total_cumulative_double
    )
    result_total["double"][key]["cumulative_probability"] = (
        total_cumulative_probability_double + result_total["double"][key]["probability"]
    )
    total_cumulative_double += result_double[key]
    total_cumulative_probability_double += result_total["double"][key]["probability"]
    result_total["double"][key]["min_value"] = (key * 2) + 1
    result_total["double"][key]["max_value"] = (key * 2) + 12
    result_total["double"][key]["middle_value"] = (key * 2) + 7


for key in result_triple:
    result_total["triple"][key] = {}
    result_total["triple"][key]["frequency"] = result_triple[key]
    result_total["triple"][key]["probability"] = float(result_triple[key]) / float(
        one_percent_all
    )
    result_total["triple"][key]["cumulative"] = (
        result_triple[key] + total_cumulative_triple
    )
    result_total["triple"][key]["cumulative_probability"] = (
        result_total["triple"][key]["probability"] + total_cumulative_probability_triple
    )
    total_cumulative_triple += result_triple[key]
    total_cumulative_probability_triple += result_total["triple"][key]["probability"]
    result_total["triple"][key]["min_value"] = key * 3

with open("diced12_double_triple.json", "w") as f:
    json.dump(result_total, f)
