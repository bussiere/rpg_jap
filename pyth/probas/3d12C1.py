import random
import json
from matplotlib import pyplot as plt
import os


result = {}
result_bis = {}
result_ter = {}
names = []
namesBis = []
namesTer = []
withk = []
withouk = []
i = 3
j = 0
while i < 37:
    result[i] = 0
    result_bis[i] = 0
    result_ter[i] = 0
    names.append(j)
    namesBis.append(j + 0.5)
    namesTer.append(j + 0.75)
    j += 1
    i += 1

counter = 1000000
i = 0


while i < counter:
    a = random.randint(1, 12)
    b = random.randint(1, 12)
    c = random.randint(1, 12)
    d = random.randint(1, 12)

    result_bis[a + b + d] += 1
    d1 = [c,d]
    d1.sort()
    d1 = d1[1]
    result[a+b+d1] += 1
    i += 1
i = 0

while i < counter:
    a = random.randint(1, 12)
    b = random.randint(1, 12)
    c = random.randint(1, 12)
    d = random.randint(1, 12)
    e = random.randint(1, 12)
    d1 = [c,d]
    d1.sort()
    d1 = d1[1]
    d2 = [a,b,e]
    d2.sort()
    d2,d3 = d2[1],d2[2]
    result_ter[d1+d2+d3] += 1
    i += 1

result_total = {}
i = 3
before = 0
before_nok = 0
before_kwithad = 0
withka = []
while i < 37:
    result_total[i] = {}
    result_total[i]["count"] = result[i]
    result_total[i]["proba"] = result[i] / (counter / 100)
    result_total[i]["cumul_proba"] = result[i] / (counter / 100) + before
    result_total[i]["count_nok"] = result_bis[i]
    result_total[i]["proba_nok"] = result_bis[i] / (counter / 100)
    result_total[i]["cumul_proba_nok"] = result_bis[i] / (counter / 100) + before_nok
    result_total[i]["count_a"] = result_ter[i]
    result_total[i]["proba_a"] = result_ter[i] / (counter / 100)
    result_total[i]["cumul_proba_a"] = result_ter[i] / (counter / 100) + before_kwithad
    before = result_total[i]["cumul_proba"]
    before_nok = result_total[i]["cumul_proba_nok"]
    before_kwithad = result_total[i]["cumul_proba_a"]
    
    withk.append(result_total[i]["proba"])
    withouk.append(result_total[i]["proba_nok"])
    withka.append(result_total[i]["proba_a"])
    i += 1
current_dir = os.path.dirname(os.path.realpath(__file__))
with open(current_dir+"/3dice12C1.json", "w") as f:
    json.dump(result_total, f)


print(withk)
plt.figure(figsize=(20, 20), dpi=25)
plt.bar(names, withouk, tick_label=names, width=0.6, color="blue")
plt.bar(namesBis, withk, tick_label=namesBis, width=0.6, color="red")
plt.bar(namesTer, withka, tick_label=namesTer, width=0.6, color="green")

plt.title("Result")
plt.xlabel("d12")
plt.ylabel("proba")

#get current dir




plt.savefig(current_dir+"/proba3d12C1.png", dpi=600)
plt.show()
plt.close()
