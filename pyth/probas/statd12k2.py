import random
import json

result = {}
result_bis = {}
i = 2
while i < 25:
    result[i] = 0
    result_bis[i] = 0
    i += 1


i = 0
counter = 1000000

while i < counter:
    a = random.randint(1, 12)
    b = random.randint(1, 12)
    c = random.randint(1, 12)
    result_bis[a + b] += 1
    if b >= c:
        result[a + b] += 1
    else:
        result[a + c] += 1
    i += 1

result_total = {}
i = 2
before = 0
before_nok = 0
while i < 25:
    result_total[i] = {}
    result_total[i]["count"] = result[i]
    result_total[i]["proba"] = result[i] / (counter / 100)
    result_total[i]["cumul_proba"] = result[i] / (counter / 100) + before
    result_total[i]["count_nok"] = result_bis[i]
    result_total[i]["proba_nok"] = result_bis[i] / (counter / 100)
    result_total[i]["cumul_proba_nok"] = result_bis[i] / (counter / 100) + before_nok
    before = result_total[i]["cumul_proba"]
    before_nok = result_total[i]["cumul_proba_nok"]
    i += 1

with open("c:/Workspace/CustomJdr/rpg_jap/pyth/dicedk12.json", "w") as f:
    json.dump(result_total, f)
