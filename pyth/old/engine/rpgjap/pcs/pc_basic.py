from rules import stats_rules
from math import floor


class PcBasic:
    def __init__(
        self,
        mind: int = 0,
        body: int = 0,
        skill: int = 0,
        strength: int = 0,
        dexterity: int = 0,
        constitution=0,
        intelligence=0,
        might=0,
        charisma=0,
        classes=[],
        first_class=None,
        second_class=None,
        fortitude=0,
        willpower=0,
        bloodied=0,
    ):
        self.mind = name
        self.body = age
        self.skill = skill
        self.strength = strength
        self.dexterity = dexterity
        self.constitution = constitution
        self.intelligence = intelligence
        self.might = might
        self.charisma = charisma
        self.classes = classes
        self.fortitude = fortitude
        self.willpower = willpower
        self.first_class = first_class
        self.first_class_level = self.first_class.level
        self.second_class = second_class
        self.second_class_level = self.second_class.level
        self.bloodied = bloodied
        self.base_evasion = 0
        self.base_defense = 0
        self.AC = 0
        self.hp = 0
        self.mp = 0


    def calc_bloodied(self):
        self.bloodied = floor(self.hp / 2)
        
    def calc_fortitude(self):
        if self.first_class.fortitude_stat_used == StatEnum.STR:
            self.fortitude_stat = self.strength
        elif self.first_class.fortitude_stat_used == StatEnum.CON:
            self.fortitude_stat = self.constitution
        elif self.first_class.fortitude_stat_used == StatEnum.DEX:
            self.fortitude_stat = self.dexterity
        elif self.first_class.fortitude_stat_used == StatEnum.INT:
            self.fortitude_stat = self.intelligence
        elif self.first_class.fortitude_stat_used == StatEnum.WILL:
            self.fortitude_stat = self.willpower
        elif self.first_class.fortitude_stat_used == StatEnum.CHA:
            self.fortitude_stat = self.charisma
        self.fortitude_stat_second = 0
        if self.second_class is not None:
            if self.second_class.fortitude_stat_used == StatEnum.STR:
                self.fortitude_stat_second = self.strength
            elif self.second_class.fortitude_stat_used == StatEnum.CON:
                self.fortitude_stat_second = self.constitution
            elif self.second_class.fortitude_stat_used == StatEnum.DEX:
                self.fortitude_stat_second = self.dexterity
            elif self.second_class.fortitude_stat_used == StatEnum.INT:
                self.fortitude_stat_second = self.intelligence
            elif self.second_class.fortitude_stat_used == StatEnum.WILL:
                self.fortitude_stat_second = self.willpower
            elif self.second_class.fortitude_stat_used == StatEnum.CHA:
                self.fortitude_stat_second = self.charisma
        self.fortitude = (
            stats_rules.bonus_stat[self.fortitude_stat]
            + self.first_class.bonus_fortitude
            + floor(stats_rules.bonus_stat[self.fortitude_stat_second] / 2)
            + floor(self.second_class.bonus_fortitude / 2)
            + self.first_class_level
            + floor(self.second_class_level/2)
        )

    def calc_willpower(self):
        if self.first_class.willpower_stat_used == StatEnum.STR:
            self.willpower_stat = self.strength
        elif self.first_class.willpower_stat_used == StatEnum.CON:
            self.willpower_stat = self.constitution
        elif self.first_class.willpower_stat_used == StatEnum.DEX:
            self.willpower_stat = self.dexterity
        elif self.first_class.willpower_stat_used == StatEnum.INT:
            self.willpower_stat = self.intelligence
        elif self.first_class.willpower_stat_used == StatEnum.WILL:
            self.willpower_stat = self.willpower
        elif self.first_class.willpower_stat_used == StatEnum.CHA:
            self.willpower_stat = self.charisma
        self.willpower_stat_second = 0
        if self.second_class is not None:
            if self.second_class.willpower_stat_used == StatEnum.STR:
                self.willpower_stat_second = self.strength
            elif self.second_class.willpower_stat_used == StatEnum.CON:
                self.willpower_stat_second = self.constitution
            elif self.second_class.willpower_stat_used == StatEnum.DEX:
                self.willpower_stat_second = self.dexterity
            elif self.second_class.willpower_stat_used == StatEnum.INT:
                self.willpower_stat_second = self.intelligence
            elif self.second_class.willpower_stat_used == StatEnum.WILL:
                self.willpower_stat_second = self.willpower
            elif self.second_class.willpower_stat_used == StatEnum.CHA:
                self.willpower_stat_second = self.charisma
        self.willpower = (
            stats_rules.bonus_stat[self.willpower_stat]
            + self.first_class.bonus_willpower
            + floor(stats_rules.bonus_stat[self.willpower_stat_second] / 2)
            + floor(self.second_class.bonus_willpower / 2)
            + self.first_class_level
            + floor(self.second_class_level/2)
        )
