class Inventory:
    def __init__(self):
        self.head = None
        self.left_hand = None
        self.right_hand = None
        self.torso = None
        self.necklace = None
        self.right_ring = None
        self.left_ring = None
        self.shoes = None
