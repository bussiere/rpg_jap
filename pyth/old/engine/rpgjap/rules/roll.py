class Roll:
    def __init__(self, dice, sides):
        self.dice = dice
        self.sides = sides

    def roll(self):
        return sum([random.randint(1, self.sides) for i in range(self.dice)])

    def roll4d12k3(self):
        dice1 = random.randint(1, 12)
        dice2 = random.randint(1, 12)
        dice3 = random.randint(1, 12)
        dice4 = random.randint(1, 12)
        if dice4 > dice3:
            dice3 = dice4
        result = {
            "RESULT": dice1 + dice2 + dice3,
            "DICE1": dice1,
            "DICE2": dice2,
            "DICE3": dice3,
            "DICE4": dice4,
        }
        return result
