class StatEnum(enum.Enum):
    STR = "strenght"
    DEX = "dexterity"
    CON = "constitution"
    INT = "intelligence"
    WILL = "willpower"
    CHA = "charisma"
