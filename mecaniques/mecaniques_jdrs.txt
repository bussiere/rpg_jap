on meca de jdr :

The Usage Die from The Black Hack. It's changed how I approach thinking about a ton of things in the same way that Fate did years ago (wherever everything can be written up like a character, including the environment).

With the UD, you've got a system that actually makes me interested in tracking inventory, rations, and torches in a dungeon. I never thought I'd say that.

For example, a torch might have a Ud8. Every fifteen minutes of real time played, the GM will call for a check on the torch. The torchbearer rolls their Ud, or 1d8. Nothing happens on a 3+, but on a 1 or 2 the torch's UD drops to 1d6. Eventually it'll drop to 1d4, and then it'll burn out entirely. It works great for arrows too!

Spells can have UD, poisons, traps, lockpicks, wands, anything that has limited use (and in OSR games, that should be almost everything) can be reached with a UD that makes it just a bit more fun.



et une autre meca de jdr interessante :
Ten Candles: Well it uses literally 10 candles as the only souce of light (is a horror rpg), when the last candle is out all the PCs are dead (is the end of the world). The dice pool of the players is decreasing and the pool of the DM is increasing, you can "push" a scene by burning some characteristic. Well is not easy to explain (specially because english is not my mother language), but you should check it. The mechanic is literally part of the ambient, tone and context of the game ( if you blow accidentaly a candle it's lost, you can't light it again). Is a very intense and personal game (like Fiasco, but grimmer). 


I'm fond of the objective system in the newest edition of Unknown Armies (3rd ed.)

Basically, you determine a group goal add a few points (x + y d10's) when you complete a task that contributes to that goal - bigger tasks add more points. When it reaches 100, you narrate how the goal is completed. You can also roll it at any time on a d100 - getting under your point total means goal is completed, while rolling over means you can't roll again for a while and lose a chunk of progress. (There's also some stuff for changing goals, scavenging what you can from the previous so you get to keep some of the accumulated points - but not all.)

I like that it keeps the party pointed at their goal, and lets them know how close they are to being done (and lets you then threaten that progress, not to mention letting them take risks to achieve things faster (which combines well with things getting worse over time or other time pressures.))


Initiative avec 3 couleurs de Dés , 1 dés chacun et on les met ala queuleue,


les dés sont donnés cela un jet sur une table