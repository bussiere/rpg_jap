from fpdf import FPDF
import json


class RpgJap(FPDF):
    def __init__(self, format):
        super().__init__(format=format)
        self.document_font = "SourceCodeProBold"
        self.blue = [76.0, 32.0, 250.0]
        self.black = [0.0, 0.0, 0.0]

    def page1(self):
        self.image("asset_data/dice/inverted-dice-1.png", 10, 8, 10)

    def tablepage(self):
        self.TABLE_COL_NAMES = ("First name", "Last name", "Age", "City")
        self.TABLE_DATA = (
            ("Jules", "Smith", "34", "San Juan"),
            ("Mary", "Ramos", "45", "Orlando"),
            ("Carlson", "Banks", "19", "Los Angeles"),
            ("Lucas", "Cimon", "31", "Angers"),
        )
        self.line_height = self.font_size * 2
        self.col_width = self.epw / 4  # distribute content evenly
        create_table(self)


def render_table_header(rpgjap: RpgJap):
    rpgjap.set_font("Times", size=16)

    rpgjap.set_font(style="B")  # enabling bold text
    for col_name in rpgjap.TABLE_COL_NAMES:
        rpgjap.cell(rpgjap.col_width, rpgjap.line_height, col_name, border=1)
    rpgjap.ln(rpgjap.line_height)
    rpgjap.set_font(style="")  # disabling bold text


def create_table(rpgjap: RpgJap):
    render_table_header(rpgjap)
    for _ in range(10):  # repeat data rows
        for row in rpgjap.TABLE_DATA:
            if rpgjap.will_page_break(rpgjap.line_height):
                render_table_header(rpgjap)
            for datum in row:
                rpgjap.cell(rpgjap.col_width, rpgjap.line_height, datum, border=1)
            rpgjap.ln(rpgjap.line_height)


pdf = RpgJap(format="A4")
pdf.add_font("SourceCodeProBold", "", "SourceCodePro-Bold.ttf", uni=True)
pdf.add_page()
pdf.page1()
pdf.add_page()
pdf.tablepage()
pdf.output("test.pdf", "F")
