 
with import <nixpkgs> {}; {
  cimgEnv = stdenv.mkDerivation {
  name = "cimgdev";
  buildInputs = [ pkgconfig stdenv cimg xorg.libX11 boost wine];
  shellHook =
  ''
    echo "Hello shell"
    export LD_LIBRARY_PATH="/nix/store/j71chi4b06bh8rpd5611d6q8asrwf9z6-opencv-4.7.0/lib/:$LD_LIBRARY_PATH";
  '';

};
environment.variables = rec {
  LD_LIBRARY_PATH="/nix/store/j71chi4b06bh8rpd5611d6q8asrwf9z6-opencv-4.7.0/lib/:LD_LIBRARY_PATH";
};
}

