#!/bin/bash
fdupes -rdN . >> result_dedup.txt
sh normalize.sh
find . -type d -empty -delete
