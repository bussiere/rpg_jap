dices = {
    18: [[6, 6, 6]],
    17: [[6, 6, 5]],
    16: [[6, 6, 4], [6, 5, 5]],
    15: [[6, 6, 3], [6, 5, 4], [5, 5, 5]],
    14: [[6, 6, 2], [6, 5, 3], [6, 4, 4], [5, 5, 4]],
    13: [[6, 6, 1], [6, 5, 2], [6, 4, 3], [5, 5, 3], [5, 4, 4]],
    12: [[6, 5, 1], [6, 4, 2], [6, 3, 3], [5, 5, 2], [5, 4, 3]],
    11: [[6, 4, 1], [6, 3, 2], [5, 5, 1], [5, 4, 2], [5, 3, 3], [4, 4, 3]],
    10: [[6, 3, 1], [6, 2, 2], [5, 4, 1], [5, 3, 2], [4, 4.2], [4, 3, 3]],
    9: [[6, 2, 1], [5, 3, 1], [5, 2, 2], [4, 4, 1], [4, 3, 2]],
    8: [[6, 1, 1], [5, 2, 1], [4, 3, 1], [4, 2, 2], [3, 3, 2]],
    7: [[5, 1, 1], [4, 2, 1], [3, 3, 1], [3, 2, 2]],
    6: [[4, 1, 1], [3, 2, 1], [2, 2, 2]],
    5: [[3, 1, 1], [2, 2, 1]],
    4: [[2, 1, 1]],
    3: [[1, 1, 1]],
}

sum_prob = 0
for key2 in dices.keys():
    result = len(dices[key2])
    sum_prob += result

dict_percent = {}
for key3 in dices.keys():
    result = len(dices[key3])
    dict_percent[key3] = result / (sum_prob / 100)

dict_percent_cumul = {}
for key3 in dices.keys():
    dict_percent_cumul[key3] = 0
    for key2 in dices.keys():
        if key2 <= key3:
            dict_percent_cumul[key3] += dict_percent[key2]


for key in dices.keys():
    print(key)
    result = len(dices[key2])
    print(result)
    print("percent")
    print(dict_percent[key])
    print("percent cumul")
    print(dict_percent_cumul[key])
    print("\n")
