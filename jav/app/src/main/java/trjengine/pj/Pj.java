package main.java.trjengine.pj;


import main.java.trjengine.escalation.EscalationDice;
import main.java.trjengine.listenum.PawnTypeEnum;
import main.java.trjengine.pawn.Pawn;
import main.java.trjengine.tools.GenerateUuid;
public class Pj extends Pawn {



    public EscalationDice escalationDice;
    public PawnTypeEnum pawnTypeEnum;


   public Pj(String name){
   	super(name);
   }

    public  Pj(){
    	this.init();
    }

    public void resetStat(){

    }

    public void init(){
    	this.pawnTypeEnum = PawnTypeEnum.PILOT;
		this.uuid = GenerateUuid.generateUuid();
		this.resetStat();
    }
}