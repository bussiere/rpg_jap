package main.java.trjengine.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import main.java.trjengine.dm.utils.StatTransmission;
import main.java.trjengine.item.Item;
import main.java.trjengine.listenum.InventoryPositionEnum;
import main.java.trjengine.pawn.Pawn;

public class InventoryPawn {

  public Map<InventoryPositionEnum, Item> mapPositionItem = new HashMap<InventoryPositionEnum, Item>();

  public void initInventory() {
    // browse all position
    for (InventoryPositionEnum position : InventoryPositionEnum.values()) {
      mapPositionItem.put(position, null);
    }
  }

  // equip all item in inventory for a pawn

    public Pawn equipAllItem(Pawn pawn) {
        // browse all position
        for (InventoryPositionEnum position : InventoryPositionEnum.values()) {
        // get item in position
        Item item = pawn.inventoryPawn.mapPositionItem.get(position);
        // equip item
        pawn = item.equipItem(pawn);
        }
        return pawn;
    }

  public Pawn equipItem(InventoryPositionEnum position,Pawn pawn) {
    // get item in position
    Item item = pawn.inventoryPawn.mapPositionItem.get(position);
    pawn = item.equipItem(pawn);

  return pawn;
  }

  public InventoryPawn() {
    initInventory();
  }

}
