package main.java.trjengine.listenum;

public enum InventoryPositionEnum {
  HEAD,
  COLLAR,
  TORSO,
  RIGHT_WRIST,
  LEFT_WRIST,
  RIGHT_HAND,
  LEFT_HAND,
  BOTH_HANDS,
  RIGHT_FINGER,
  LEFT_FINGER,
  BOOTS,
  CAPE,
}
