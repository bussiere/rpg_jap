package main.java.trjengine.listenum;
public enum DamageTypeEnum {
	LASER,
	KINETIC,
	MISSILE,
    PHYSICAL,
}