package main.java.trjengine.listenum;

public enum StatusPositiveEnum {
    BLURRED,
    BLESSED,
    EXALTED,
    BERSERK,
    HASTED,
    INSPIRED,
    INVISIBLE,
    PROTECTED,
    REGENERATED,
    RESISTANT,
    STRENGTHENED,
    VIGILANT,
    VITALIZED,
    SHIELDED,
    NONE
}
