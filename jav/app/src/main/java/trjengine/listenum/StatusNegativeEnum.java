package main.java.trjengine.listenum;

public enum StatusNegativeEnum {

    BLIND,
    BLEEDING,
    BURNING,
    CHARMED,
    CONFUSED,
    CURSED,
    DEAFENED,
    DISEASED,
    EXHAUSTED,
    FRIGHTENED,
    GRAPPLED,
    INCAPACITATED,
    INVISIBLE,
    PARALYZED,
    PETRIFIED,
    POISONED,

    FROZEN,
    SLOWED,

    NONE

	
}