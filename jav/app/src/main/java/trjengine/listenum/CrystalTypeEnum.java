package main.java.trjengine.listenum;
/*

Light Element Light.png

Strong against Shadow. Resistant to Light and Shadow. Weak to Toxin.

    An example of a playable character with this element would be Klein.


Shadow Element Shadow.png

Strong against Flora. Resistant to Shadow and Flora. Weak to Light.

    An example of a playable character with this element would be Troz.


Flora Element Flora.png

Strong against Water. Resistant to Flora and Water. Weak to Shadow.

    An example of a playable character with this element would be Shep.


Water Element Water.png

Strong against Fire. Resistant to Water and Fire. Weak to Flora.

    An example of a playable character with this element would be Pike.


Fire Element Fire.png

Strong against Ice. Resistant to Fire and Ice. Weak to Water.

    An example of a playable character with this element would be Io.


Ice Element Ice.png

Strong against Earth. Resistant to Ice and Earth. Weak to Fire.

    An example of a playable character with this element would be Nyx.


Earth Element Earth.png

Strong against Volt. Resistant to Earth and Volt. Weak to Ice.

    Examples of playable characters with this element would be Chip and Blitz.


Volt Element Thunder.png

Strong against Wind. Resistant to Volt and Wind. Weak to Earth.

    An example of a playable character with this element would be Rex.


Wind Element Wind.png

Strong against Toxin. Resistant to Wind and Toxin. Weak to Volt.

    An example of a playable character with this element would be Kon.


Toxin Element Poison-0.png

Strong against Light. Resistant to Toxin and Light. Weak to Wind.

    Examples of playable characters with this element would be Luca and Tate.




*/
public enum CrystalTypeEnum {

    WATER,
    FIRE,
    ICE,
    EARTH,
    WIND,
    ELECTRICITY,
    POISON,
    LIGHT,
    DARK,
    BLOOD,
    MECHANICAL,
    FLORA,
    CRYSTAL,
    METAL,
    VOID,
    LIFE,
    NEUTRAL,
    NONE,
    
}
