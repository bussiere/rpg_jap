package main.java.trjengine.listenum;

public enum WeaponMechaSlotEnum {
    RIGHT_HAND,
    LEFT_HAND,
    TWO_HAND,
    RIGHT_OR_LEFT_HAND,
    BACK,
}
