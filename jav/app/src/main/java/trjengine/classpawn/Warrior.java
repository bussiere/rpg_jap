package main.java.trjengine.classpawn;

import main.java.trjengine.listenum.ClassEnum;

public class Warrior extends ClassPawn {

  public Warrior() {
    super();
    this.name = "Warrior";
    this.classEnum = ClassEnum.WARRIOR;
    this.description =
      "A warrior is a fighter who uses physical strength and skill to fight.";
    this.level = 2;
    this.levelCurrent = this.level;
  }
}
