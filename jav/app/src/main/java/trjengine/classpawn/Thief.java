package main.java.trjengine.classpawn;

import main.java.trjengine.listenum.ClassEnum;

public class Thief extends ClassPawn {

  public Thief() {
    super();
    this.name = "Thief";
    this.description =
      "A thief is a person who steals another person's property, especially by stealth and without using force or threat of violence.";
    this.classEnum = ClassEnum.THIEF;
    this.level = 1;
    this.levelCurrent = this.level;
  }
}
