package main.java.trjengine.classpawn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import main.java.trjengine.dm.table.CaracBonus;
import main.java.trjengine.listenum.ClassEnum;

public class ClassPawn {

  public String name;
  public String description;
  public int level;

  public int levelCurrent;

  public ClassEnum classEnum;

  public static CaracBonus caracBonus = new CaracBonus();

  public int hpBase;

  public float goldenNumber = 1.2f;

  public int evasionBase;
  public int acBonus;

  public Map<Integer, Integer> evasionBonus = new HashMap<Integer, Integer>();
  public Map<Integer, Integer> hpBonus = new HashMap<Integer, Integer>();
  public Map<Integer, Integer> mightBonus = new HashMap<Integer, Integer>();
  public Map<Integer, Integer> resistanceBonus = new HashMap<Integer, Integer>();
  public Map<Integer, Integer> lifePoolBonus = new HashMap<Integer, Integer>();
  public Map<Integer, Integer> mindPoolBonus = new HashMap<Integer, Integer>();
  public Map<Integer, Integer> moralBonus = new HashMap<Integer, Integer>();
  public Map<Integer, Integer> healthPointBonus = new HashMap<Integer, Integer>();
  // initbonus
  public Map<Integer, Integer> initiativeBonus = new HashMap<Integer, Integer>();

  public Map<Integer, Integer> acBonusPercing = new HashMap<Integer, Integer>();
  public Map<Integer, Integer> acBonusSlashing = new HashMap<Integer, Integer>();
  public Map<Integer, Integer> acBonusBlunt = new HashMap<Integer, Integer>();

  public int getCaracBonusForMight(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return caracBonus.getBonus(will);
  }

  public int getCaracBonusForMight(int will) {
    return CaracBonus.getBonus(will);
  }

  public int getClassBonusForMight(int level) {
    return mightBonus.get(level);
  }

  public int caracUsedForMight(int will) {
    return will;
  }

  public int calculateMight(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return (
      caracUsedForMight(will) +
      getClassBonusForMight(this.levelCurrent) +
      getCaracBonusForMight(will)
    );
  }

  public int getCaracBonusForResistance(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return caracBonus.getBonus(constitution);
  }

  public int getCaracBonusForResistance(int constitution) {
    return caracBonus.getBonus(constitution);
  }

  public int getClassBonusForResistance(int level) {
    return resistanceBonus.get(level);
  }

  public int calculateResistance(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return (
      getClassBonusForResistance(this.levelCurrent) +
      getCaracBonusForResistance(constitution)
    );
  }

  public int calculateMoral(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return (
      getClassBonusForMoral(this.levelCurrent) + getCaracBonusForMoral(will)
    );
  }

  public int getCaracBonusForMoral(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return caracBonus.getBonus(will);
  }

  public int getCaracBonusForMoral(int will) {
    return caracBonus.getBonus(will);
  }

  public int getClassBonusForMoral(int level) {
    return moralBonus.get(levelCurrent);
  }

  public int caracUsedForInit(
        int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return (int) ((dexterity + will) / 2);
  }

  public int caracUsedForInit(int dexterity, int will) {
    return (int) ((dexterity + will) / 2);
  }

  public int caracUsedForMight(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return will;
  }

  public int calculateInitScore(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    System.out.println("intiiative base");
    // print dex
    System.out.println("dexterity");
    System.out.println("dexterity");
    System.out.println(dexterity);
    // print will
    System.out.println("will");
    System.out.println(will);
    System.out.println("(int) ((dexterity + will) /2)");
    System.out.println((int) ((dexterity + will) /2));
    // print getCaracBonusForInit
    System.out.println("getCaracBonusForInit((int) ((dexterity + will) /2))");
    System.out.println(getCaracBonusForInit((int) ((dexterity + will) /2)));
    // print getClassBonusForInit
    System.out.println("getClassBonusForInit(this.levelCurrent)");
    System.out.println(getClassBonusForInit(this.levelCurrent));
    return (
      caracUsedForInit(dexterity,will) +
      getClassBonusForInit(this.levelCurrent) +
      getCaracBonusForInit((int) ((dexterity + will) / 2))
    );
  }

  public int getCaracBonusForInit(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return CaracBonus.getBonus(dexterity);
  }

  public int getCaracBonusForInit(int dexterity) {
    return CaracBonus.getBonus(dexterity);
  }

  public int getClassBonusForInit(int level) {
    return initiativeBonus.get(level);
  }

  public int calculateHP() {
    return this.hpBase + hpBonus.get(this.levelCurrent);
  }

  public int getCaracBonusForEvasion(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return caracBonus.getBonus(dexterity);
  }

  public int getCaracBonusForEvasion(int dexterity) {
    return caracBonus.getBonus(dexterity);
  }

  public int getClassBonusForEvasion(int level) {
    return evasionBonus.get(level);
  }

  public int caracUsedForEvasion(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return (int) ((dexterity + intelligence) / 2);
  }


    public int caracUsedForEvasion(
    int dexterity,
    int intelligence
  ) {
    return (int) ((dexterity + intelligence) / 2);
  }

  public int calculateEvasion(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    System.out.println("intelligence");
    System.out.println("intelligence");
    System.out.println(intelligence);
    System.out.println("dexterity");
    System.out.println("dexterity");
    System.out.println("dexterity");
    System.out.println(dexterity);
    // print class bonus
    System.out.println("getClassBonusForEvasion(this.levelCurrent)");
    System.out.println("getClassBonusForEvasion(this.levelCurrent)");
    System.out.println(getClassBonusForEvasion(this.levelCurrent));
    // print get carac bonus
    System.out.println("getCaracBonusForEvasion(dexterity)");
    System.out.println("getCaracBonusForEvasion(dexterity)");
    System.out.println(getCaracBonusForEvasion(dexterity));
    // print  dexterity + intelligence / 2
    System.out.println("(int)((dexterity+intelligence)/2)");
    System.out.println("(int) ((dexterity + intelligence) / 2)");
    System.out.println((int) ((dexterity + intelligence) / 2));

    return (
        caracUsedForEvasion(dexterity,intelligence) +
        getClassBonusForEvasion(this.levelCurrent) +
        getCaracBonusForEvasion((int) ((dexterity + intelligence) / 2))
    );
  }

  

  public int calculateHealthPoint(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return (
      getClassBonusForHealthPoint(this.levelCurrent) +
      getCaracBonusForHealthPoint(constitution)
    );
  }

  public int getCaracBonusForHealthPoint(int constitution) {
    return caracBonus.getBonus(constitution);
  }

  public int getCaracBonusForHealthPoint(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return CaracBonus.getBonus(constitution);
  }

  public int caracUsedForLifePool(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return (int) ((constitution + will) / 2);
  }

  public int getClassBonusForHealthPoint(int level) {
    return healthPointBonus.get(levelCurrent);
  }

  public int calculateLifePool(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return (
      getClassBonusForLifePool(this.levelCurrent) +
      getCaracBonusForLifePool(constitution) 
    );
  }

  public int getCaracBonusForLifePool(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return caracBonus.getBonus(constitution);
  }

  public int getCaracBonusForLifePool(int constitution) {
    return CaracBonus.getBonus(constitution);
  }

  public int getClassBonusForLifePool(int level) {
    return lifePoolBonus.get(levelCurrent);
  }

  public int caracUsedForMindPool(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return will;
  }
  public int calculateMindPool(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return (
      getClassBonusForMindPool(this.levelCurrent) +
      getCaracBonusForMindPool(will)
    );
  }

  public int calculateMindPoolSecondary(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return (
      getClassBonusForMindPool(this.levelCurrent) +
      getCaracBonusForMindPool(will)
    );
  }

  public int getCaracBonusForMindPool(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    return caracBonus.getBonus(will);
  }

  public int getCaracBonusForMindPool(int will) {
    return caracBonus.getBonus(will);
  }

  public int getClassBonusForMindPool(int level) {
    return mindPoolBonus.get(level);
  }

  public void initBonus() {
    List<Map<Integer, Integer>> listBonus = new ArrayList<Map<Integer, Integer>>();
    listBonus.add(hpBonus);
    listBonus.add(mightBonus);
    listBonus.add(resistanceBonus);
    listBonus.add(lifePoolBonus);
    listBonus.add(mindPoolBonus);
    listBonus.add(moralBonus);
    listBonus.add(acBonusPercing);
    listBonus.add(acBonusSlashing);
    listBonus.add(acBonusBlunt);
    listBonus.add(evasionBonus);
    listBonus.add(healthPointBonus);
    listBonus.add(initiativeBonus);
    int j;
    float k;
    for (Map<Integer, Integer> mapBonus : listBonus) {
      j = 0;
      k = 1f;
      for (int i = 1; i < 21; i++) {
        mapBonus.put(i, j);
        k = k * this.goldenNumber;
        j = (int) k;
      }
    }
    System.out.println("healthPointBonus");
    System.out.println("healthPointBonus");
    System.out.println(healthPointBonus);
  }

  public ClassPawn() {
    this.level = 1;
    this.levelCurrent = this.level;
    initBonus();
  }
}
