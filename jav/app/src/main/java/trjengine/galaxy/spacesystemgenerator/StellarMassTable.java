package main.java.trjengine.galaxy.spacesystemgenerator;


import java.util.List;
import java.util.ArrayList;

public class StellarMassTable{

    public class StellarMassTableEntry{
        public int first3d6;
        public int second3d6Min;
        public int second3d6Max;
        public double mass;
    }

    public static List<StellarMassTableEntry> stellarMassTable = new ArrayList<StellarMassTableEntry>();

    public void initStellarMassTable(){
        StellarMassTableEntry entry = new StellarMassTableEntry();
        entry.first3d6 = 3;
        entry.second3d6Min = 3;
        entry.second3d6Max = 10;
        entry.mass = 2.00;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 3;
        entry.second3d6Min = 11;
        entry.second3d6Max = 36;
        entry.mass = 1.90;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 4;
        entry.second3d6Min = 3;
        entry.second3d6Max = 8;
        entry.mass = 1.80;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 4;
        entry.second3d6Min = 9;
        entry.second3d6Max = 11;
        entry.mass = 1.70;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 4;
        entry.second3d6Min = 12;
        entry.second3d6Max = 36;
        entry.mass = 1.60;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 5;
        entry.second3d6Min = 3;
        entry.second3d6Max = 7;
        entry.mass = 1.50;
        stellarMassTable.add(entry);


        entry = new StellarMassTableEntry();
        entry.first3d6 = 5;
        entry.second3d6Min = 8;
        entry.second3d6Max = 10;
        entry.mass = 1.45;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 5;
        entry.second3d6Min = 11;
        entry.second3d6Max = 12;
        entry.mass = 1.40;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 5;
        entry.second3d6Min = 13;
        entry.second3d6Max = 36;
        entry.mass = 1.35;

        entry = new StellarMassTableEntry();
        entry.first3d6 = 6;
        entry.second3d6Min = 3;
        entry.second3d6Max = 7;
        entry.mass = 1.30;

        entry = new StellarMassTableEntry();
         entry.first3d6 = 6;
        entry.second3d6Min = 8;
        entry.second3d6Max = 9;
        entry.mass = 1.25;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 6;
        entry.second3d6Min = 10;
        entry.second3d6Max = 10;
        entry.mass = 1.20;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 6;
        entry.second3d6Min = 11;
        entry.second3d6Max = 12;
        entry.mass = 1.15;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 6;
        entry.second3d6Min = 13;
        entry.second3d6Max = 36;
        entry.mass = 1.10;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 7;
        entry.second3d6Min = 3;
        entry.second3d6Max = 7;
        entry.mass = 1.05;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 7;
        entry.second3d6Min = 8;
        entry.second3d6Max = 9;
        entry.mass = 1.00;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 7;
        entry.second3d6Min = 10;
        entry.second3d6Max = 10;
        entry.mass = 0.95;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 7;
        entry.second3d6Min = 11;
        entry.second3d6Max = 12;
        entry.mass = 0.90;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 7;
        entry.second3d6Min = 13;
        entry.second3d6Max = 36;
        entry.mass = 0.85;

        entry = new StellarMassTableEntry();
        entry.first3d6 = 8;
        entry.second3d6Min = 3;
        entry.second3d6Max = 7;
        entry.mass = 0.80;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 8;
        entry.second3d6Min = 8;
        entry.second3d6Max = 9;
        entry.mass = 0.75;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 8;
        entry.second3d6Min = 10;
        entry.second3d6Max = 10;
        entry.mass = 0.70;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 8;
        entry.second3d6Min = 11;
        entry.second3d6Max = 12;
        entry.mass = 0.65;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 8;
        entry.second3d6Min = 13;
        entry.second3d6Max = 36;
        entry.mass = 0.60;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 9;
        entry.second3d6Min = 3;
        entry.second3d6Max = 8;
        entry.mass = 0.55;

        entry = new StellarMassTableEntry();
        entry.first3d6 = 9;
        entry.second3d6Min = 9;
        entry.second3d6Max = 11;
        entry.mass = 0.50;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 9;
        entry.second3d6Min = 12;
        entry.second3d6Max = 36;
        entry.mass = 0.45;
        stellarMassTable.add(entry);



        entry = new StellarMassTableEntry();
        entry.first3d6 = 10;
        entry.second3d6Min = 3;
        entry.second3d6Max = 8;
        entry.mass = 0.40;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 10;
        entry.second3d6Min = 9;
        entry.second3d6Max = 11;
        entry.mass = 0.35;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 10;
        entry.second3d6Min = 12;
        entry.second3d6Max = 36;
        entry.mass = 0.30;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 11;
        entry.second3d6Min = 3;
        entry.second3d6Max = 36;
        entry.mass = 0.25;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 12;
        entry.second3d6Min = 3;
        entry.second3d6Max = 36;
        entry.mass = 0.20;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 13;
        entry.second3d6Min = 3;
        entry.second3d6Max = 36;
        entry.mass = 0.15;
        stellarMassTable.add(entry);

        entry = new StellarMassTableEntry();
        entry.first3d6 = 14;
        entry.second3d6Min = 3;
        entry.second3d6Max = 36;
        entry.mass = 0.15;
        stellarMassTable.add(entry);

        for (int i =0; i < 10; i++){
            entry = new StellarMassTableEntry();
            entry.first3d6 = 15+i;
            entry.second3d6Min = 3;
            entry.second3d6Max = 36;
            entry.mass = 0.10-(i*0.0001);
            stellarMassTable.add(entry);
        }




    }

    public StellarMassTable(){
        initStellarMassTable();
    }

    public double getStellarMassTable(int first3d6, int second3d6){
        for (StellarMassTableEntry entry : stellarMassTable){
            if (first3d6 == entry.first3d6){
                if (second3d6 >= entry.second3d6Min && second3d6 <= entry.second3d6Max){
                    return entry.mass;
                }
            }
        }
        return 0;
    }

}