package main.java.trjengine.galaxy.spacesystemgenerator;

import main.java.trjengine.galaxy.Star;
import main.java.trjengine.galaxy.SpaceSystem;
import java.util.List;
import java.util.ArrayList;

import main.java.trjengine.galaxy.galaxyenum.StellarClassificationEnum;
import main.java.trjengine.galaxy.galaxyenum.SpaceSystemAgeEnum;

import main.java.trjengine.galaxy.spacesystemgenerator.StellarEvolutionTable;
import main.java.trjengine.galaxy.spacesystemgenerator.StellarMassTable;


public class SpaceSystemGenerator{

    public SpaceSystem spaceSystem = new SpaceSystem();
    public static StellarEvolutionTable stellarEvolutionTable = new StellarEvolutionTable();
    public static StellarMassTable stellarMassTable = new StellarMassTable();

    public int getD6(){
        return (int)(Math.random()*6)+1;
    }
    //3-18
    public int get3D6(){
        return (int)(Math.random()*15)+1+3;
    }
    public int getD6MinusOne(){
        return getD6()-1;
    }


    public double[] getBaseAgeStepAStepBFromAgeEnum(SpaceSystemAgeEnum ageEnum){
        double baseAge = 0;
        double stepA = 0;
        double stepB = 0;
        switch (ageEnum){
            case EXTREME_POPULATION_I:
                baseAge = 0.001*(Math.random()*9);
                stepA = 0.001*(Math.random()*9);
                stepB = 0.001*(Math.random()*9);
                break;
            case YOUNg_POPULATION_I:
                baseAge = 0.1;
                stepA = 0.3;
                stepB = 0.05;
                break;
            case INTERMEDIATE_POPULATION_I:
                baseAge = 2;
                stepA = 0.6;
                stepB = 0.1;
                break;
            case OLD_POPULATION_I:
                baseAge = 10;
                stepA = 0.6;
                stepB = 0.1;
                break;
            case INTERMEDIATE_POPULATION_II:
                baseAge = 13.6;
                stepA = 0.6;
                stepB = 0.1;
                break;
            case EXTREME_POPULATION_II:
                baseAge = 18;
                stepA = 0.6;
                stepB = 0.1;
                break;
        }
        return new double[]{baseAge, stepA, stepB};
    }

    public void calculateAgeSpaceSystem(){

        int d3 = get3D6();
        System.out.println("d3: "+d3);
        if (d3 == 3){
            spaceSystem.ageEnum = SpaceSystemAgeEnum.EXTREME_POPULATION_I;
        } else {
            if (d3 <= 6){
                spaceSystem.ageEnum = SpaceSystemAgeEnum.YOUNg_POPULATION_I;
            } else {
                if (d3 <= 10){
                    spaceSystem.ageEnum = SpaceSystemAgeEnum.INTERMEDIATE_POPULATION_I;
                } else {
                    if (d3 <= 14){
                        spaceSystem.ageEnum = SpaceSystemAgeEnum.OLD_POPULATION_I;
                    } else {
                        if (d3 <= 17){
                            spaceSystem.ageEnum = SpaceSystemAgeEnum.INTERMEDIATE_POPULATION_II;
                        } else {
                            spaceSystem.ageEnum = SpaceSystemAgeEnum.EXTREME_POPULATION_II;
                        }
                    }
                }
            }
        }
        double baseAge, stepA, stepB;
        double[] result = getBaseAgeStepAStepBFromAgeEnum(spaceSystem.ageEnum);
        baseAge = result[0];
        stepA = result[1];
        stepB = result[2];
        int d6_1 = getD6();
        int d6_2 = getD6();
        System.out.println("d6_1: "+d6_1);
        System.out.println("d6_2: "+d6_2);
        spaceSystem.age = baseAge + stepA * (d6_1-1)+ stepB * (d6_2-1);

    }



    

    public int getNumberOfStar(int d3d6){

        // 3d6
        //3-10 1 star
        //11-15 2 stars
        //16-18+ 3 stars

        if (d3d6 <=10){
            return 1;
        } else {
            if (d3d6 <=15){
                return 2;
            } else {
                return 3;
            }
        }
    }


    public void calulateStellarEvolution(){
        for (Star star: spaceSystem.stars){
            StellarEvolutionTable.StellarEvolutionTableEntry entry = stellarEvolutionTable.getTableDataFromMass(star.stellarMass);
            if (entry == null){
                System.out.println("Error: entry is null");
                System.out.println("star.stellarMass: "+star.stellarMass);
                entry = stellarEvolutionTable.getTableDataFromMass(0.1);

            }
            
            star.spectralTypeEnum = entry.spectralTypeEnum;
            star.tempKelvin = entry.tempKelvin;
            star.tempCelsius = entry.tempCelsius;
            star.tempFahrenheit = entry.tempFahrenheit;
            star.lMin = entry.lMin;
            star.lMax = entry.lMax;
            star.mSpan = entry.mSpan;
            star.sSpan = entry.sSpan;
            star.gSpan = entry.gSpan;
        }

    }
    
    public void generateSpaceSystem(){

        System.out.println("Generating galaxy...");
        System.out.println("get3D6()");
        System.out.println(get3D6());
        int numberOfStar = getNumberOfStar(get3D6());
        System.out.println("Number of star: " + numberOfStar);
        int previousd3d6=0;

        for (int i=0; i<numberOfStar; i++){
            Star star = new Star();
            star.name = "Star " + i;
            star.classificationName = "Star " + i;
            star.classification = null;
            int first3d6 = get3D6();
            if (previousd3d6 != 0) {
                if (first3d6 == 1) {
                    first3d6 = previousd3d6;
                } else {
                    first3d6 += previousd3d6;
                }
            }
            star.stellarMass = stellarMassTable.getStellarMassTable(first3d6, get3D6());
            spaceSystem.stars.add(star);
            previousd3d6 = first3d6;
        }

        calculateAgeSpaceSystem();
        calulateStellarEvolution();

        System.out.println("Age: " + spaceSystem.age);
        System.out.println("Age Enum: " + spaceSystem.ageEnum);
        for (Star star: spaceSystem.stars){
            System.out.println("Star: " + star.name);
            System.out.println("Stellar Mass: " + star.stellarMass);
            System.out.println("Spectral Type: " + star.spectralTypeEnum);
            System.out.println("Temperature Kelvin: " + star.tempKelvin);
            System.out.println("Temperature Celsius: " + star.tempCelsius);
            System.out.println("Temperature Fahrenheit: " + star.tempFahrenheit);
            System.out.println("Luminosity Min: " + star.lMin);
            System.out.println("Luminosity Max: " + star.lMax);
            System.out.println("Mass Span: " + star.mSpan);
            System.out.println("Spectral Span: " + star.sSpan);
            System.out.println("Luminosity Span: " + star.gSpan);
        }



    }



}