package main.java.trjengine.galaxy.spacesystemgenerator;

import java.util.ArrayList;
import java.util.List;

import main.java.trjengine.galaxy.galaxyenum.SpectralTypeEnum;
import main.java.trjengine.tools.MetricConversion;

public class StellarEvolutionTable {

  public static List<StellarEvolutionTableEntry> table = new ArrayList<StellarEvolutionTableEntry>();

  public class StellarEvolutionTableEntry {

    public double mass;
    public SpectralTypeEnum spectralTypeEnum;
    public double tempKelvin;
    public double tempCelsius;
    public double tempFahrenheit;
    public double lMin;
    public double lMax;
    public double mSpan;
    public double sSpan;
    public double gSpan;
  }

  public void initializeTable() {
    StellarEvolutionTableEntry entry = new StellarEvolutionTableEntry();
    entry.mass = 0.10;
    entry.spectralTypeEnum = SpectralTypeEnum.M7;
    entry.tempKelvin = 3100;
    entry.lMin = 0.0012;
    entry.lMax = 0;
    entry.mSpan = 0;
    entry.sSpan = 0;
    entry.gSpan = 0;
    table.add(entry);

    entry = new StellarEvolutionTableEntry();
    entry.mass = 0.15;
    entry.spectralTypeEnum = SpectralTypeEnum.M6;
    entry.tempKelvin = 3200;
    entry.lMin = 0.0036;
    entry.lMax = 0;
    entry.mSpan = 0;
    entry.sSpan = 0;
    entry.gSpan = 0;
    table.add(entry);

    entry = new StellarEvolutionTableEntry();
    entry.mass = 0.20;
    entry.spectralTypeEnum = SpectralTypeEnum.M5;
    entry.tempKelvin = 3200;
    entry.lMin = 0.0079;
    entry.lMax = 0;
    entry.mSpan = 0;
    entry.sSpan = 0;
    entry.gSpan = 0;
    table.add(entry);

    entry = new StellarEvolutionTableEntry();
    entry.mass = 0.25;
    entry.spectralTypeEnum = SpectralTypeEnum.M4;
    entry.tempKelvin = 3300;
    entry.lMin = 0.015;
    entry.lMax = 0;
    entry.mSpan = 0;
    entry.sSpan = 0;
    entry.gSpan = 0;
    table.add(entry);

    entry = new StellarEvolutionTableEntry();
    entry.mass = 0.30;
    entry.spectralTypeEnum = SpectralTypeEnum.M4;
    entry.tempKelvin = 3300;
    entry.lMin = 0.024;
    entry.lMax = 0;
    entry.mSpan = 0;
    entry.sSpan = 0;
    entry.gSpan = 0;
    table.add(entry);

    entry = new StellarEvolutionTableEntry();
    entry.mass = 0.35;
    entry.spectralTypeEnum = SpectralTypeEnum.M3;
    entry.tempKelvin = 3400;
    entry.lMin = 0.037;
    entry.lMax = 0;
    entry.mSpan = 0;
    entry.sSpan = 0;
    entry.gSpan = 0;
    table.add(entry);

    entry = new StellarEvolutionTableEntry();
    entry.mass = 0.40;
    entry.spectralTypeEnum = SpectralTypeEnum.M2;
    entry.tempKelvin = 3500;
    entry.lMin = 0.054;
    entry.lMax = 0;
    entry.mSpan = 0;
    entry.sSpan = 0;
    entry.gSpan = 0;
    table.add(entry);

    entry = new StellarEvolutionTableEntry();
    entry.mass = 0.45;
    entry.spectralTypeEnum = SpectralTypeEnum.M1;
    entry.tempKelvin = 3600;
    entry.lMin = 0.07;
    entry.lMax = 0.08;
    entry.mSpan = 70;
    entry.sSpan = 0;
    entry.gSpan = 0;
    table.add(entry);

    entry = new StellarEvolutionTableEntry();
    entry.mass = 0.50;
    entry.spectralTypeEnum = SpectralTypeEnum.M0;
    entry.tempKelvin = 3800;
    entry.lMin = 0.09;
    entry.lMax = 0.11;
    entry.mSpan = 59;
    entry.sSpan = 0;
    entry.gSpan = 0;
    table.add(entry);

    entry = new StellarEvolutionTableEntry();
    entry.mass = 0.55;
    entry.spectralTypeEnum = SpectralTypeEnum.K8;
    entry.tempKelvin = 4000;
    entry.lMin = 0.11;
    entry.lMax = 0.15;
    entry.mSpan = 50;
    entry.sSpan = 0;
    entry.gSpan = 0;
    table.add(entry);

    entry = new StellarEvolutionTableEntry();
    entry.mass = 0.60;
    entry.spectralTypeEnum = SpectralTypeEnum.K6;
    entry.tempKelvin = 4200;
    entry.lMin = 0.13;
    entry.lMax = 0.20;
    entry.mSpan = 42;
    entry.sSpan = 0;
    entry.gSpan = 0;
    table.add(entry);

    entry = new StellarEvolutionTableEntry();
    entry.mass = 0.65;
    entry.spectralTypeEnum = SpectralTypeEnum.K5;
    entry.tempKelvin = 4400;
    entry.lMin = 0.15;
    entry.lMax = 0.25;
    entry.mSpan = 37;
    entry.sSpan = 0;
    entry.gSpan = 0;
    table.add(entry);

    addCelsiusAndFahrenheitToTable();
  }

  public static void addCelsiusAndFahrenheitToTable() {
    for (StellarEvolutionTableEntry entry : table) {
      entry.tempCelsius =
        MetricConversion.convertKelvinToCelsius(entry.tempKelvin);
      entry.tempFahrenheit =
        MetricConversion.convertKelvinToFahrenheit(entry.tempKelvin);
    }
  }

  public StellarEvolutionTableEntry getTableDataFromMass(double mass) {
    if (mass < 0.10) {
      mass = 0.10;
    }
    for (StellarEvolutionTableEntry entry : table) {
      if (entry.mass == mass) {
        return entry;
      }
    }
    return null;
  }

  public StellarEvolutionTableEntry getTableDataFromSpectralType(
    SpectralTypeEnum spectralTypeEnum
  ) {
    for (StellarEvolutionTableEntry entry : table) {
      if (entry.spectralTypeEnum == spectralTypeEnum) {
        return entry;
      }
    }
    return null;
  }

  public StellarEvolutionTable() {
    initializeTable();
  }
  /*   
    
    Mass
 Type
 Temp
 L-Min
 L-Max
 M-Span
 S-Span
 G-Span
0.10
 M7
 3,100
 0.0012
 –
 –
 –
 –
0.15
 M6
 3,200
 0.0036
 –
 –
 –
 –
0.20
 M5
 3,200
 0.0079
 –
 –
 –
 –
0.25
 M4
 3,300
 0.015
 –
 –
 –
 –
0.30
 M4
 3,300
 0.024
 –
 –
 –
 –
0.35
 M3
 3,400
 0.037
 –
 –
 –
 –
0.40
 M2
 3,500
 0.054
 –
 –
 –
 –
0.45
 M1
 3,600
 0.07
 0.08
 70
 –
 –
0.50
 M0
 3,800
 0.09
 0.11
 59
 –
 –
0.55
 K8
 4,000
 0.11
 0.15
 50
 –
 –
0.60
 K6
 4,200
 0.13
 0.20
 42
 –
 –
0.65
 K5
 4,400
 0.15
 0.25
 37
 –
 –
0.70
 K4
 4,600
 0.19
 0.35
 30
 –
 –
0.75
 K2
 4,900
 0.23
 0.48
 24
 –
 –
0.80
 K0
 5,200
 0.28
 0.65
 20
 –
 –
0.85
 G8
 5,400
 0.36
 0.84
 17
 –
 –
0.90
 G6
 5,500
 0.45
 1.0
 14
 –
 –
0.95
 G4
 5,700
 0.56
 1.3
 12
 1.8
 1.1
1.00
 G2
 5,800
 0.68
 1.6
 10
 1.6
 1.0
1.05
 G1
 5,900
 0.87
 1.9
 8.8
 1.4
 0.8
1.10
 G0
 6,000
 1.1
 2.2
 7.7
 1.2
 0.7
1.15
 F9
 6,100
 1.4
 2.6
 6.7
 1.0
 0.6
1.20
 F8
 6,300
 1.7
 3.0
 5.9
 0.9
 0.6
1.25
 F7
 6,400
 2.1
 3.5
 5.2
 0.8
 0.5
1.30
 F6
 6,500
 2.5
 3.9
 4.6
 0.7
 0.4
1.35
 F5
 6,600
 3.1
 4.5
 4.1
 0.6
 0.4
1.40
 F4
 6,700
 3.7
 5.1
 3.7
 0.6
 0.4
1.45
 F3
 6,900
 4.3
 5.7
 3.3
 0.5
 0.3
1.50
 F2
 7,000
 5.1
 6.5
 3.0
 0.5
 0.3
1.60
 F0
 7,300
 6.7
 8.2
 2.5
 0.4
 0.2
1.70
 A9
 7,500
 8.6
 10
 2.1
 0.3
 0.2
1.80
 A7
 7,800
 11
 13
 1.8
 0.3
 0.2
1.90
 A6
 8,000
 13
 16
 1.5
 0.2
 0.1
2.00
 A5
 8,200
 16
 20
 1.3
 0.2
 0.1

    
    
    */

}
