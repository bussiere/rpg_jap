package main.java.trjengine.galaxy.galaxyenum;

public enum SpaceObjectEnum {
    STAR,PLANET,MOON,ASTEROID,COMET,NEBULA,GALAXY,CLUSTER,QUASAR,BLACK_HOLE,WHITE_DWARF,NEUTRON_STAR

}