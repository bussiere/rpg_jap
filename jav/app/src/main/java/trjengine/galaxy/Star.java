package main.java.trjengine.galaxy;
import main.java.trjengine.galaxy.galaxyenum.StellarClassificationEnum;
import main.java.trjengine.galaxy.galaxyenum.SpaceSystemAgeEnum;
import main.java.trjengine.galaxy.galaxyenum.SpectralTypeEnum;


public class Star {
    public String name;
    public String classificationName;
    public StellarClassificationEnum classification;
    public double stellarMass;
    public SpectralTypeEnum spectralTypeEnum;
    public double tempKelvin;
    public double tempCelsius;
    public double tempFahrenheit;
    public double lMin;
    public double lMax;
    public double mSpan;
    public double sSpan;
    public double gSpan;


    public Star(){

    }
}