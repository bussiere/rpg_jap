package main.java.trjengine.galaxy;
import java.util.List;
import java.util.ArrayList;
import main.java.trjengine.galaxy.Star;
import main.java.trjengine.galaxy.galaxyenum.SpaceSystemAgeEnum;
public class SpaceSystem {

    public String name;
    public String classificationName;

    public SpaceSystemAgeEnum ageEnum;
    public double age;
    public List <Star> stars = new ArrayList<Star>();



    public SpaceSystem(){

    }

}