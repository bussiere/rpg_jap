package main.java.trjengine.mech;

import main.java.trjengine.item.MechPart;
import main.java.trjengine.item.WeaponMecha;
import main.java.trjengine.item.Crystal;
import main.java.trjengine.pilot.Pilot;
import main.java.trjengine.pawn.Pawn;
import main.java.trjengine.pawn.PawnMech;
import main.java.trjengine.listenum.PawnTypeEnum;
import main.java.trjengine.listenum.MechFormEnum;

public class Mech extends PawnMech {
	

	public MechPart head;
	public MechPart torso;
	public MechPart rightHand;
	public MechPart leftHand;
	public MechPart rightLeg;
	public MechPart leftLeg;

	public Crystal crystal;




	public WeaponMecha rightHandWeaponMecha;
	public WeaponMecha leftHandWeaponMecha;
	public WeaponMecha backWeaponMecha;


	public  int  shieldResistanceSlotLaser;
	public  int  shieldResistanceSlotKinetic;
	public  int  shieldResistanceSlotMissile;

	public MechFormEnum mechFormEnum;

	public  int  Energy;


    public  int  Health;




    
   
    public  int  PowernessCurrent;
	public  int  ManiabilityCurrent;
	public  int  ProcessingCurrent;
	public  int  VisionCurrent;
	public  int  MovementCurrent;
	public  int  ArmorCurrent;
	public  int  HealthCurrent;
	public  int  ReactionCurrent;

	// pour le bonus de charism aux unités adjacente
	public  int  ComLinkCurrent;

	public  int  EnergyCurrent;


    public  int  LastTurnFormChange;

	public  int  Form1Powerness;
	public  int  Form1Maniability;
	public  int  Form1Processing;
	public  int  Form1Movement;
	public  int  Form1Vision;
	// pour le bonus de charism aux unités adjacente
	public  int  Form1ComLink;
    public  int  Form1EnergyConsumption;


	public  int  Form1MechStrength;
	public  int  Form1MechDexterity;
	public  int  Form1MechIntelligence;
	public  int  Form1MechWill;
	public  int  Form1MechCharisma;
	public  int  Form1Mechperception;
	public  int  Form1Mechspeed;
	public  int  Form1Mechluck;


	public  int  Form2Powerness;
	public  int  Form2Maniability;
	public  int  Form2Processing;
	public  int  Form2Movement;
	// pour le bonus de charism aux unités adjacente
	public  int  Form2ComLink;
	public  int  Form2EnergyConsumption;


	public  int  Form2MechStrength;
	public  int  Form2MechDexterity;
	public  int  Form2MechIntelligence;
	public  int  Form2MechWill;
	public  int  Form2MechCharisma;
	public  int  Form2Mechperception;
	public  int  Form2Mechspeed;
	public  int  Form2Mechluck;
	


	public  int  Form3Powerness;
	public  int  Form3Maniability;
	public  int  Form3Processing;
	public  int  Form3Movement;
	// pour le bonus de charism aux unités adjacente
	public  int  Form3ComLink;
	public  int  Form3EnergyConsumption;


	public  int  Form3MechStrength;
	public  int  Form3MechDexterity;
	public  int  Form3MechIntelligence;
	public  int  Form3MechWill;
	public  int  Form3MechCharisma;
	public  int  Form3Mechperception;
	public  int  Form3Mechspeed;
	public  int  Form3Mechluck;


	public Pilot pilot;

	public  int  MechStrength;
	public  int  MechDexterity;
	public  int  MechIntelligence;
	public  int  MechWill;
	public  int  MechCharisma;
	public  int  Mechperception;
	public  int  Mechspeed;
	public  int  Mechluck;


	public  int  MechStrengthCurrent;
	public  int  MechDexterityCurrent;
	public  int  MechIntelligenceCurrent;
	public  int  MechWillCurrent;
	public  int  MechCharismaCurrent;
	public  int  MechperceptionCurrent;
	public  int  MechspeedCurrent;
	public  int  MechluckCurrent;


	public  int  meleeAttackScoreCurrent ;
	public  int  rangeAttackScoreCurrent;
	public  int  evasionScoreCurrent ;
	public  int  initScoreCurrent ;
	public  int  criticalScoreCurrent;
	public  int  presenceScoreCurrent ;

	public void transformMechForm(MechFormEnum mechFormEnum) {
		this.mechFormEnum = mechFormEnum;
		if (mechFormEnum == MechFormEnum.FORM1){
			this.MechStrengthCurrent += Form1MechStrength;
			this.MechDexterityCurrent += Form1MechDexterity;
			this.MechIntelligenceCurrent += Form1MechIntelligence;
			this.MechWillCurrent += Form1MechWill;
			this.MechCharismaCurrent += Form1MechCharisma;
			this.MechperceptionCurrent += Form1Mechperception;
			this.MechspeedCurrent += Form1Mechspeed;
			this.MechluckCurrent += Form1Mechluck;
		} else if (mechFormEnum == MechFormEnum.FORM2){
			this.MechStrengthCurrent += Form2MechStrength;
			this.MechDexterityCurrent += Form2MechDexterity;
			this.MechIntelligenceCurrent += Form2MechIntelligence;
			this.MechWillCurrent += Form2MechWill;
			this.MechCharismaCurrent += Form2MechCharisma;
			this.MechperceptionCurrent += Form2Mechperception;
			this.MechspeedCurrent += Form2Mechspeed;
			this.MechluckCurrent += Form2Mechluck;
		} else if (mechFormEnum == MechFormEnum.FORM3){
			this.MechStrengthCurrent += Form3MechStrength;
			this.MechDexterityCurrent += Form3MechDexterity;
			this.MechIntelligenceCurrent += Form3MechIntelligence;
			this.MechWillCurrent += Form3MechWill;
			this.MechCharismaCurrent += Form3MechCharisma;
			this.MechperceptionCurrent += Form3Mechperception;
			this.MechspeedCurrent += Form3Mechspeed;
			this.MechluckCurrent += Form3Mechluck;

		}
	}

	public Mech(String name) {
		super(name);
		this.pawnTypeEnum = PawnTypeEnum.MECH;
		this.mechFormEnum = MechFormEnum.FORM1;
	}
	public Mech() {
		super();
		this.pawnTypeEnum = PawnTypeEnum.MECH;
		this.mechFormEnum = MechFormEnum.FORM1;
	}


    public void recalcMechStat(){

    	this.MechStrengthCurrent += (this.pilot.strengthCurrent/100)*this.PowernessCurrent;
    	this.MechDexterityCurrent += (this.pilot.dexterityCurrent/100)*this.ManiabilityCurrent;
    	this.MechIntelligenceCurrent += (this.pilot.intelligenceCurrent/100)*this.ProcessingCurrent;
    	this.MechWillCurrent += (this.pilot.willCurrent/100)*this.ProcessingCurrent;
    	this.MechspeedCurrent += (this.pilot.speedCurrent/100)*this.ReactionCurrent;
    	this.MechperceptionCurrent += (this.pilot.perceptionCurrent/100)*this.VisionCurrent;
    	this.MechCharismaCurrent += (this.pilot.charismaCurrent/100)*this.ComLinkCurrent;
    }
	public void sitPilot(Pilot pilot) {
		this.pilot = pilot;
		transformMechForm(this.mechFormEnum);
		recalcMechStat();
		recalcStatCombat();
	}
	public void escalatePilot(Pilot pilot) {
		this.pilot = pilot;
		this.pilot.resetStat();
		this.pilot.escalate();
		transformMechForm(this.mechFormEnum);
		recalcMechStat();
		recalcStatCombat();
	}


	public void recalcStatCombat() {

		this.meleeAttackScoreCurrent = ((this.MechStrength*6+this.MechDexterity*3+this.pilot.luck)/10);
		this.rangeAttackScoreCurrent = ((this.Mechperception*6+this.MechDexterity*3+this.pilot.luck)/10);
		this.evasionScoreCurrent = ((this.Mechspeed*6+this.MechIntelligence*3+this.pilot.luck)/30);
		this.initScoreCurrent = ((this.Mechspeed*6+this.MechWill*3+this.MechIntelligence)/10);
		this.criticalScoreCurrent = ((this.MechWill*3+this.pilot.luck)/10);
		this.presenceScoreCurrent = ((this.MechCharisma*6+this.MechWill*3)/9);
	}

	public void printMech(){
		System.out.println("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°");
		System.out.println("Mech : " + this.name);
		System.out.println("Pilot : " + this.pilot.name);
		System.out.println("Pilot Stat");
		this.pilot.printPilot();
		System.out.println("Form : " + this.mechFormEnum);
		System.out.println("Powerness : " + this.PowernessCurrent);
		System.out.println("Maniability : " + this.ManiabilityCurrent);
		System.out.println("Processing : " + this.ProcessingCurrent);
		System.out.println("Reaction : " + this.ReactionCurrent);
		System.out.println("Vision : " + this.VisionCurrent);
		System.out.println("ComLink : " + this.ComLinkCurrent);
		System.out.println("Strength : " + this.MechStrength);
		System.out.println("Dexterity : " + this.MechDexterity);
		System.out.println("Intelligence : " + this.MechIntelligence);
		System.out.println("Will : " + this.MechWill);
		System.out.println("Charisma : " + this.MechCharisma);
		System.out.println("perception : " + this.Mechperception);
		System.out.println("speed : " + this.Mechspeed);
		System.out.println("luck : " + this.Mechluck);
		System.out.println("Melee Attack Score : " + this.meleeAttackScore);
		System.out.println("Range Attack Score : " + this.rangeAttackScore);
		System.out.println("Evasion Score : " + this.evasionScore);
		System.out.println("Init Score : " + this.initScore);
		System.out.println("Critical Score : " + this.criticalScore);
		System.out.println("Presence Score : " + this.presenceScore);
		System.out.println("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°");

	}
}