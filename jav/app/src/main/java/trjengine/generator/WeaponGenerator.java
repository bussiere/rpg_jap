package main.java.trjengine.generator;

import java.util.Random;
import java.util.ArrayList;
// import weapon 
import main.java.trjengine.item.Weapon;

import main.java.trjengine.tools.ReadFile;


import main.java.trjengine.listenum.WeaponCategoryEnum;
// import jackson 
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;




public class WeaponGenerator {
    
    public static class WeaponName {
        public ArrayList<String> weapon_name;
    }
    
    public Weapon weapon = new Weapon();

    public void WeaponGeneratorByType(WeaponCategoryEnum weaponCategoryEnum) {
        weapon.weaponCategoryEnum = weaponCategoryEnum;
        if (weaponCategoryEnum == WeaponCategoryEnum.AXE) {
            weapon.damageMin = 10;
            weapon.damageMax = 10;
            weapon.bonusToHit = 0;
        }
    
        
    }

    
    public WeaponGenerator(int level) {
        
    }
    

    public Weapon generateWeapon(){
                // read json file to get weapon name 
        
        
        /*  ressource/json/weapon_name.json      
        {
            "weapon_name":["Excalibur","Caliburn","Durandal","Joyeuse","Kusanagi","Zulfiqar","Hauteclere","Balmung","Dainsleif","Tyrfing","Gram","Nothung","Tizona","Colada","Hrunting","Nagamitsu","Kogarasumaru","Kusanagi","Kotetsu","Masamune","Muramasa","Kusana"]
        }
        */
        
        ReadFile readFile = new ReadFile();
        ObjectMapper mapper = new ObjectMapper();
        try {
            WeaponName weaponName = mapper.readValue(readFile.getFile("json/Weapon.json"), new TypeReference<WeaponName>() {
            });
            Random rand = new Random();
            int randomIndex = rand.nextInt(weaponName.weapon_name.size());
            weapon.name = weaponName.weapon_name.get(randomIndex);
            
            
        } catch (Exception e) {
            // raise e
            throw new RuntimeException(e);
            //e.printStackTrace();
        }
        System.out.println("Weapon name :");
        System.out.println("weapon.name");
        System.out.println(weapon.name);
        // read json file to get weapon type

        return weapon;
    }

    public WeaponGenerator() {
        



        
        
    }
    
}