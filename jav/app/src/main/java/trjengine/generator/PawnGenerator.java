package main.java.trjengine.generator;

import main.java.trjengine.classpawn.ClassPawn;
import main.java.trjengine.classpawn.Thief;
import main.java.trjengine.classpawn.Warrior;
import main.java.trjengine.dm.Dice;
import main.java.trjengine.dm.table.CaracBonus;
import main.java.trjengine.listenum.PawnTypeEnum;
import main.java.trjengine.pawn.Pawn;

public class PawnGenerator {

  public static Dice dice = new Dice();

  public Pawn Generate() {
    Pawn pawn = new Pawn("toto");
    return Generate(pawn);
  }

  public Pawn Generate(Pawn pawn) {
    pawn = GenerateCarac(pawn);
    return pawn;
  }

  public Pawn GenerateAverage() {
    Pawn pawn = new Pawn("toto");
    return GenerateAverage(pawn);
  }

  public Pawn GenerateAverage(Pawn pawn) {
    pawn = GenerateCaracAverage(pawn);
    return pawn;
  }

  public Pawn GenerateCaracAverage(Pawn pawn) {
    pawn.strength = 16;
    pawn.strengthCurrent = pawn.strength;
    // dexterity
    pawn.dexterity = 16;
    pawn.dexterityCurrent = pawn.dexterity;
    // constitution
    pawn.constitution = 16;
    pawn.constitutionCurrent = pawn.constitution;
    // intelligence
    pawn.intelligence = 16;
    pawn.intelligenceCurrent = pawn.intelligence;
    // will
    pawn.will = 16;
    pawn.willCurrent = pawn.will;
    // charisma
    pawn.charisma = 16;
    pawn.charismaCurrent = pawn.charisma;
    pawn.generatePostCarac();
    return pawn;
  }

  public Pawn GenerateCarac(Pawn pawn) {
    // fr each caracteristic 1D20

    // strength

    pawn.strength = dice.averageRoll(3, 12);
    pawn.strengthCurrent = pawn.strength;
    // dexterity
    pawn.dexterity = dice.averageRoll(3, 12);
    pawn.dexterityCurrent = pawn.dexterity;
    // constitution
    pawn.constitution = dice.averageRoll(3, 12);
    pawn.constitutionCurrent = pawn.constitution;
    // intelligence
    pawn.intelligence = dice.averageRoll(3, 12);
    pawn.intelligenceCurrent = pawn.intelligence;
    // will
    pawn.will = dice.averageRoll(3, 12);
    pawn.willCurrent = pawn.will;
    // charisma
    pawn.charisma = dice.averageRoll(3, 12);
    pawn.charismaCurrent = pawn.charisma;
    pawn.generatePostCarac();
    return pawn;
  }
}
