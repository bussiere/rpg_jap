package main.java.trjengine.generator;


import main.java.trjengine.inventory.InventoryPawn;
import main.java.trjengine.pawn.Pawn;
import main.java.trjengine.listenum.ClassEnum;
import main.java.trjengine.listenum.InventoryPositionEnum;
import main.java.trjengine.item.WeaponMecha;
import main.java.trjengine.item.Weapon;
import main.java.trjengine.generator.WeaponGenerator;


public class InventoryPawnGenerator {

    public InventoryPawn inventoryPawn;

    public void createInventoryPawn(){
        // create inventory
        inventoryPawn = new InventoryPawn();
        // create weapon
        Weapon weapon = new WeaponGenerator().generateWeapon();
        // add weapon to inventory
        inventoryPawn.mapPositionItem.put(InventoryPositionEnum.RIGHT_HAND, weapon);

    }
    
    public InventoryPawnGenerator(){
        // print hello world
        System.out.println("line 29");
        System.out.println("Hello World");
        createInventoryPawn();


            
            
    }
    
    
}