package main.java.trjengine.spell;

import main.java.trjengine.pawn.Pawn;

public class Spell {

    public int id;
    public String name;
    public String description;
    public int level;
    public int range;
    public int area;
    public int duration;
    public int cooldown;
    public int castTime;


    public Pawn affectePawn(Pawn pawn){
        return pawn;
    }
}
