package main.java.trjengine.tools;

import com.google.common.io.Resources;
// import ARRAYLIST
import java.util.ArrayList;
// import file reader
import java.io.FileReader;
// import URL for file reader
import java.net.URL;
// import file not found exception
import java.io.FileNotFoundException;
// import input stream
import java.io.InputStream;
// import input stream reader
import java.io.InputStreamReader;


public class ReadFile {
    
    
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
    
    public  InputStreamReader getFile(String path){
        URL url = Resources.getResource(path);
        try {
            
            InputStream is = getClass().getClassLoader().getResourceAsStream(url.getPath());
            return new InputStreamReader(is);
        } catch (Exception e) {
            System.out.println("Using second path");
            System.out.println("e");
            System.out.println(e);
            InputStream is = getClass().getClassLoader().getResourceAsStream(path);
            return new InputStreamReader(is);
        }
    }
}
