package main.java.trjengine.tools;


public class MetricConversion {

    public static double convertKelvinToCelsius(double kelvin){
        return kelvin - 273.15;
    }

    public static double convertKelvinToFahrenheit(double kelvin){
        return (kelvin - 273.15) * 9/5 + 32;
    }

    public static double convertCelsiusToKelvin(double celsius){
        return celsius + 273.15;
    }

    public static double convertFahrenheitToKelvin(double fahrenheit){
        return (fahrenheit - 32) * 5/9 + 273.15;
    }

}