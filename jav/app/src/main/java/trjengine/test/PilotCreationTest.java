package main.java.trjengine.test;

import main.java.trjengine.pilot.Pilot;
import main.java.trjengine.mech.Mech;
import main.java.trjengine.item.WeaponMecha;

public class PilotCreationTest{
	
    public  PilotCreationTest(){

	Pilot aur = new Pilot();
    aur.strength = 12;
    aur.dexterity = 12;
    aur.constitution = 12;
    aur.intelligence = 12;
    aur.will = 12;
    aur.charisma = 12;
    aur.perception = 12;
    aur.speed = 12;
    aur.luck = 12;
    aur.moral = 12;
    
    Mech macross = new Mech();
    
    macross.PowernessCurrent = 80;
    macross.ManiabilityCurrent= 80;
    macross.ProcessingCurrent= 80;
    macross.VisionCurrent= 80;
    macross.MovementCurrent= 80;
    macross.ArmorCurrent= 80;
    macross.HealthCurrent= 80;
    macross.ReactionCurrent= 80;
    macross.ComLinkCurrent= 80;
    macross.sitPilot(aur);
    
    
    WeaponMecha weaponMecha = new WeaponMecha();
    weaponMecha.dammageMin = 1;
    weaponMecha.dammageMax = 16;
    weaponMecha.WeaponMechaInit();
    
    
    System.out.println("CouCou");
    
    System.out.println("MechStrength");
    System.out.println("macross.MechStrength");
    System.out.println(macross.MechStrength);
    System.out.println("MechDexterity");
    System.out.println("macross.MechDexterity");
    System.out.println(macross.MechDexterity);
    System.out.println("MechIntelligence");
    System.out.println("macross.MechIntelligence");
    System.out.println(macross.MechIntelligence);
    System.out.println("MechWill");
    System.out.println("macross.MechWill");
    System.out.println(macross.MechWill);
    System.out.println("Mechspeed");
    System.out.println("macross.Mechspeed");
    System.out.println(macross.Mechspeed);
    System.out.println("Mechperception");
    System.out.println("macross.Mechperception");
    System.out.println(macross.Mechperception);
    System.out.println("MechCharisma");
    System.out.println("macross.MechCharisma");
    System.out.println(macross.MechCharisma);
    
    System.out.println("meleeAttackScore");
    System.out.println("macross.meleeAttackScore");
    System.out.println(macross.meleeAttackScore);
    System.out.println("rangeAttackScore");
    System.out.println("macross.rangeAttackScore");
    System.out.println(macross.rangeAttackScore);
    System.out.println("initScore");
    System.out.println("macross.initScore");
    System.out.println(macross.initScore);
    System.out.println("evasionScore");
    System.out.println("macross.evasionScore");
    System.out.println(macross.evasionScore);
    System.out.println("criticalScore");
    System.out.println("macross.criticalScore");
    System.out.println(macross.criticalScore);
    System.out.println("presenceScore");
    System.out.println("macross.presenceScore");
    System.out.println(macross.presenceScore);
    
    System.out.println("weaponMecha.dammageMin");
    System.out.println("weaponMecha.dammageMin");
    System.out.println(weaponMecha.dammageMin);
    System.out.println("weaponMecha.dammageMax");
    System.out.println("weaponMecha.dammageMax");
    System.out.println(weaponMecha.dammageMax);
    System.out.println("weaponMecha.dammageCrit");
    System.out.println("weaponMecha.dammageCrit");
    System.out.println(weaponMecha.dammageCrit);
    System.out.println("weaponMecha.dammageRange");
    System.out.println("weaponMecha.dammageRange");
    System.out.println(weaponMecha.dammageRange);
    System.out.println("weaponMecha.dammageBoost");
    System.out.println("weaponMecha.dammageBoost");
    System.out.println(weaponMecha.dammageBoost);
    System.out.println("weaponMecha.printDammageRange()");
    weaponMecha.printDammageRange();   
    }

}