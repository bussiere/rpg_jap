package main.java.trjengine.test;

import java.util.List;
import java.util.ArrayList;

import main.java.trjengine.dm.combat.*;
import main.java.trjengine.dm.combat.Combat.InitOrder;
import main.java.trjengine.dm.table.*;
import main.java.trjengine.pawn.Pawn;
import main.java.trjengine.generator.PawnGenerator;
import main.java.trjengine.inventory.InventoryPawn;
import main.java.trjengine.generator.InventoryPawnGenerator;
import main.java.trjengine.item.Weapon;
import main.java.trjengine.generator.WeaponGenerator;
import main.java.trjengine.listenum.InventoryPositionEnum;


public class TestCombat {
  public static BonusMalus bonusMalus = new BonusMalus();

  public static PawnGenerator pg = new PawnGenerator();

  public static WeaponGenerator  wg = new WeaponGenerator();

  CombatRoll combatRoll = new CombatRoll();

    static List<Pawn> team1 = new ArrayList<Pawn>();
    static List<Pawn> team2 = new ArrayList<Pawn>();


  public static void TestInitiative(Pawn pawn1, Pawn pawn2){
    System.out.println("ligne 32");
    System.out.println("TestInitiative");
    System.out.println("Pawn1:");
    pawn1.printCarac();
    pawn1.printClass();
    pawn1.printPostCarac();
    System.out.println("Pawn2:");
    pawn2.printCarac();
    pawn2.printClass();
    pawn2.printPostCarac();

    team1.add(pawn1);
    team2.add(pawn2);
    System.out.println("ligne 45");
    Combat.rollIinit(team1,team2);
    for (InitOrder element : Combat.listInitOrder) {
      System.out.println("ligne 48");
      System.out.println("element.pawn.name +  + element.init");
      System.out.println(element.pawn.name + " " + element.init);
    }
  }

  public void Battle(){
    int turnCombat = 1;
    System.out.println("ligne 55");
        for (InitOrder element : Combat.listInitOrder) {
          System.out.println("element.pawn.name +  init :   + element.init");
          System.out.println(element.pawn.name + " init :  " + element.init);
      // print pawn turn order
      System.out.println("TurnCombat " + turnCombat + " " + element.pawn.name);
      turnCombat++;
    }

  }

  public void Turns() {
    int nbTurn = 10;
    System.out.println("ligne 67");
    System.out.println("Turns");
    int currentTurn =1;
    while (currentTurn <= nbTurn) {
      System.out.println("Turn " + currentTurn);
      currentTurn++;
      Battle();
    }

  }

  public void Run() {
    Pawn pawn1 = pg.GenerateAverage();
    WeaponGenerator wg = new WeaponGenerator();
    Weapon weapon = wg.generateWeapon();
    System.out.println("line 82");
    pawn1.inventoryPawn = new InventoryPawnGenerator().inventoryPawn;
    // equip weapon todo in pawn


    
    pawn1.name = "Pawn1";
    System.out.println("TestCombat");
    System.out.println("Pawn1:");
    pawn1.printCarac();
    pawn1.printClass();
    pawn1.printPostCarac();
    System.out.println("Pawn2:");
    Pawn pawn2 = pg.GenerateAverage();
    pawn2.name = "Pawn2";
    pawn2.printCarac();
    pawn2.printClass();
    pawn2.printPostCarac();
    System.out.println("ligne 99");
    TestInitiative(pawn1, pawn2);
    System.out.println("ligne 101");
    Turns();
    
    System.out.println("TestCombat");
    // weapon name
    System.out.println("Weapon name: " + pawn1.inventoryPawn.mapPositionItem.get(InventoryPositionEnum.RIGHT_HAND).name);
  }
}
