package main.java.trjengine.squad;

import main.java.trjengine.listenum.FactionEnum;
import main.java.trjengine.mech.Mech;
import java.util.List;
import java.util.ArrayList;
import main.java.trjengine.pawn.Pawn;
import main.java.trjengine.listenum.OrientationEnum;
import main.java.trjengine.listenum.SquadPositionEnum;
import main.java.trjengine.exception.PositionNotFoundException;
import main.java.trjengine.tools.GenerateUuid;
/*
public enum SquadPositionEnum {
    N,
    NE,
    E,
    SE,
    S,
    SW,
    W,
    NW,
    CENTER,
    
}
*/
public class Squad extends Pawn {
    
    public String name;
    public String description;
    public FactionEnum faction;
    public String uuid;
    public int  leaderWeight;
    
    
    public Mech mech1;
    public Mech mech2;
    public Mech mech3;
    public Mech mech4;
    public Mech mech5;
    public Mech mech6;
    public Mech mech7;
    public Mech mech8;
    public Mech mech9;

    public OrientationEnum orientationEnum;
    
    public String LeaderUuid;
    
    public List<Mech> mechs;
    public String[] mechPositionSquad;
    
    public float [] squadPositionBonusDamage;
    
    public int   SquadStrength;
    public int   SquadDexterity;
    public int   SquadIntelligence;
    public int   SquadWill;
    public int   SquadCharisma;
    public int   Squadperception;
    public int   Squadspeed;
    public int   Squadluck;

    public int   meleeAttackScore;

    public int   rangeAttackScore;

    public int   evasionScore;

    public int   initScore;

    public int   presenceScore;

    public int   posY;
    public int   posX;

    public String hexagonUuid;
    
    
    
    public void initialize(){
        this.uuid = GenerateUuid.generateUuid();
        this.leaderWeight = 3;
        this.mechPositionSquad = new String[9];
        this.squadPositionBonusDamage = new float[]{1.1f,1.1f,1.1f,1,1,1,0.9f,0.9f,0.9f};
        
    }
    
    public Squad() {
        initialize();
        
    }
    
    public Squad(String name, String description, FactionEnum faction) {
        this.name = name;
        this.description = description;
        this.faction = faction;
        initialize();
    }
    public Mech getMechFromSquadWithUuid(String mechUuid) {
        for (Mech mech : this.mechs) {
            if (mech.uuid == mechUuid) {
                return mech;
            }
        }     
        return null;
    }
    
    public void removeMech(Mech mech){
        if (this.mech1.uuid.equals(mech.uuid)) {
            this.mech1 = null;
            
        }else if (this.mech2.uuid.equals(mech.uuid)) {
            this.mech2 = null;
            
        } else if (this.mech3.uuid.equals(mech.uuid)) {
            this.mech3 = null;
            
        } else if (this.mech4.uuid.equals(mech.uuid)) {
            this.mech4 = null;
            
        } else if (this.mech5.uuid.equals(mech.uuid)) {
            this.mech5 = null;
            
        } else if (this.mech6.uuid.equals(mech.uuid)) {
            this.mech6 = null;
            
        } else if (this.mech7.uuid.equals(mech.uuid)) {
            this.mech7 = null;
            
        } else if (this.mech8.uuid.equals(mech.uuid)) {
            this.mech8 = null;
            
        } else if (this.mech9.uuid.equals(mech.uuid)) {
            this.mech9 = null;
            
        }
        for (Mech mechTemp : this.mechs) {
            int   count =0;
            if (mechTemp.uuid == mech.uuid) {
                this.mechs.set(count,null);
                break;
            }
            count++;
        }     
        for (int i=0;i < this.mechPositionSquad.length;i++){
            if (mech.uuid ==  this.mechPositionSquad[i]){
                this.mechPositionSquad[i] = null;
                break;
            }
        }
    }
    
    public void  updateMechFromSquadWithMech(Mech mechToUpdate) {
        int   count = 0;
        for (Mech mech : this.mechs) {
            if (mech.uuid == mechToUpdate.uuid) {
                this.mechs.set(count,mechToUpdate);
            }
            count++;
        }     
    }
    
    public void calcSquadStat(){
        int   count = 0;
        for (Mech mech : this.mechs) {
            count++;
            if (mech != null) {
                if (this.LeaderUuid != mech.uuid){
                    this.SquadStrength += mech.MechStrength;
                    this.SquadDexterity += mech.MechDexterity;
                    this.SquadIntelligence += mech.MechIntelligence;
                    this.SquadWill += mech.MechWill;
                    this.SquadCharisma += mech.MechCharisma;
                    this.Squadperception += mech.Mechperception;
                    this.Squadspeed += mech.Mechspeed;
                    this.Squadluck += mech.Mechluck;
                    
                    this.meleeAttackScore += mech.meleeAttackScore;
                    this.rangeAttackScore += mech.rangeAttackScore;
                    this.evasionScore += mech.evasionScore;
                    this.initScore += mech.initScore;
                    this.presenceScore += mech.presenceScore;
                } else {
                    this.SquadStrength += mech.MechStrength * this.leaderWeight;
                    this.SquadDexterity += mech.MechDexterity * this.leaderWeight;
                    this.SquadIntelligence += mech.MechIntelligence * this.leaderWeight;
                    this.SquadWill += mech.MechWill * this.leaderWeight;
                    this.SquadCharisma += mech.MechCharisma * this.leaderWeight;
                    this.Squadperception += mech.Mechperception * this.leaderWeight;
                    this.Squadspeed += mech.Mechspeed * this.leaderWeight;
                    this.Squadluck += mech.Mechluck * this.leaderWeight;
                    this.meleeAttackScore += mech.meleeAttackScore * this.leaderWeight;
                    this.rangeAttackScore += mech.rangeAttackScore * this.leaderWeight;
                    this.evasionScore += mech.evasionScore * this.leaderWeight;
                    this.initScore += mech.initScore * this.leaderWeight;
                    this.presenceScore += mech.presenceScore * this.leaderWeight;
                }
            }
        }
        count = count -1;
        count += this.leaderWeight;
        this.SquadStrength = this.SquadStrength / count;
        this.SquadDexterity = this.SquadDexterity / count;
        this.SquadIntelligence = this.SquadIntelligence / count;
        this.SquadWill = this.SquadWill / count;
        this.SquadCharisma = this.SquadCharisma / count;
        this.Squadperception = this.Squadperception / count;
        this.Squadspeed = this.Squadspeed / count;
        this.Squadluck = this.Squadluck / count;
        this.meleeAttackScore = this.meleeAttackScore / count;
        this.rangeAttackScore = this.rangeAttackScore / count;
        this.evasionScore = this.evasionScore / count;
        this.initScore = this.initScore / count;
        this.presenceScore = this.presenceScore / count;
        
        
        
    }
    public void changePositionMechInSquad(SquadPositionEnum squadPositionEnum,Mech mech) throws PositionNotFoundException{
        for (int i=0;i < this.mechPositionSquad.length;i++){
            if (mech.uuid ==  getMechFromPosition(squadPositionEnum).uuid){
                this.mechPositionSquad[i] = null;
            }
        }
        this.mechPositionSquad[getPositionSquadFromSquadPositionEnum(squadPositionEnum)] = mech.uuid;
    }
    public SquadPositionEnum getPositionEnumSquadFromSquadArrayPosition(int positionArray) throws  PositionNotFoundException{
        //TODO 
        if (positionArray ==0){
            return SquadPositionEnum.NW;
        } else if (positionArray ==1){
            return SquadPositionEnum.N;
        } else if (positionArray == 2) {
            return SquadPositionEnum.NE ;
        } else if (positionArray == 3) {
            return SquadPositionEnum.W;
        }else if (positionArray == 4) {
            return SquadPositionEnum.CENTER;
        } else if (positionArray == 5) {
            return SquadPositionEnum.E;
        }else if (positionArray == 6) {
            return SquadPositionEnum.SW;
        } else if (positionArray == 7) {
            return SquadPositionEnum.S;
        } else if (positionArray == 8) {
            return SquadPositionEnum.SE;
        }
        throw new PositionNotFoundException("Empty String");
    }
    
    public int   getPositionArrayFromMech(Mech mech) throws PositionNotFoundException{
        for (int i =0;i < this.mechPositionSquad.length;i++) {
            if (this.mechPositionSquad[i] == mech.uuid){
                return i;
            }
        }
        throw new PositionNotFoundException("Empty String");
        
        
    }
    
    public SquadPositionEnum getPositionEnumArrayFromMech(Mech mech ) throws PositionNotFoundException{
        return getPositionEnumSquadFromSquadArrayPosition(getPositionArrayFromMech(mech));
        
    }
    public int   getPositionSquadFromSquadPositionEnum(SquadPositionEnum squadPositionEnum) throws PositionNotFoundException{
        if (squadPositionEnum == SquadPositionEnum.NW){
            return 0;
        } else if (squadPositionEnum == SquadPositionEnum.N){
            return 1;
        } else if (squadPositionEnum == SquadPositionEnum.NE) {
            return 2;
        } else if (squadPositionEnum == SquadPositionEnum.W) {
            return 3;
        }else if (squadPositionEnum == SquadPositionEnum.CENTER) {
            return 4;
        } else if (squadPositionEnum == SquadPositionEnum.E) {
            return 5;
        }else if (squadPositionEnum == SquadPositionEnum.SW) {
            return 6;
        } else if (squadPositionEnum == SquadPositionEnum.S) {
            return 7;
        } else if (squadPositionEnum == SquadPositionEnum.SE) {
            return 8;
        }
        throw new PositionNotFoundException("Empty String");
    }
    
    
    public Mech getMechFromPosition(SquadPositionEnum squadPositionEnum)  throws PositionNotFoundException {
        String mechUuid = this.mechPositionSquad[getPositionSquadFromSquadPositionEnum(squadPositionEnum)];
        for (Mech mech : this.mechs) {
            if (mech.uuid == mechUuid) {
                return mech;
            }
        }     
        return null;
    }
    public String getMechUuidFromPosition(SquadPositionEnum squadPositionEnum) throws PositionNotFoundException{
        return this.mechPositionSquad[getPositionSquadFromSquadPositionEnum(squadPositionEnum)];
    }

    public void updateMechSquad(){
        int   count = 0;
        if (this.mech1 != null) {
            this.mechs.add(this.mech1);
            
        }
        if (this.mech2 != null) {
            this.mechs.add(this.mech2);
            
        }
        
        if (this.mech3 != null) {
            this.mechs.add(this.mech3);
            
        }
        if (this.mech4 != null) {
            this.mechs.add(this.mech4);
            
        }
        if (this.mech5 != null) {
            this.mechs.add(this.mech5);
            
        }
        if (this.mech6 != null) {
            this.mechs.add(this.mech6);
            this.mechPositionSquad[count] = this.mech6.uuid;
            count ++;
        }
        if (this.mech7 != null) {
            this.mechs.add(this.mech7);
            
        }
        if (this.mech8 != null) {
            this.mechs.add(this.mech8);
            
        }
        if (this.mech9 != null) {
            this.mechs.add(this.mech9);
            
        }
    }

    public void buildSquadWithoutPositionning() {
        this.mechs = new ArrayList<Mech>();
        updateMechSquad();
    }
    public void buildSquad() {
        this.mechs = new ArrayList<Mech>();
        int   count = 0;
        if (this.mech1 != null) {
            this.mechs.add(this.mech1);
            this.mechPositionSquad[count] = this.mech1.uuid;
            count ++;
        }
        if (this.mech2 != null) {
            this.mechs.add(this.mech2);
            this.mechPositionSquad[count] = this.mech2.uuid;
            count ++;
        }
        
        if (this.mech3 != null) {
            this.mechs.add(this.mech3);
            this.mechPositionSquad[count] = this.mech3.uuid;
            count ++;
        }
        if (this.mech4 != null) {
            this.mechs.add(this.mech4);
            this.mechPositionSquad[count] = this.mech4.uuid;
            count ++;
        }
        if (this.mech5 != null) {
            this.mechs.add(this.mech5);
            this.mechPositionSquad[count] = this.mech5.uuid;
            count ++;
        }
        if (this.mech6 != null) {
            this.mechs.add(this.mech6);
            this.mechPositionSquad[count] = this.mech6.uuid;
            count ++;
        }
        if (this.mech7 != null) {
            this.mechs.add(this.mech7);
            this.mechPositionSquad[count] = this.mech7.uuid;
            count ++;
        }
        if (this.mech8 != null) {
            this.mechs.add(this.mech8);
            this.mechPositionSquad[count] = this.mech8.uuid;
            count ++;
        }
        if (this.mech9 != null) {
            this.mechs.add(this.mech9);
            this.mechPositionSquad[count] = this.mech9.uuid;
            count ++;
        }
    }
    
    public void  setLeader(Mech mech) {
        this.LeaderUuid = mech.uuid;
        
    }
    
    public Mech getLeader() {
        for (Mech mech : mechs) {
            if (mech.uuid == this.LeaderUuid) {
                return mech;
            }
        }
        return null;
    }
    
    public void recalcInitiative() {
        this.initiative = 0;
        for (Mech mech : mechs) {
            if (this.LeaderUuid != mech.uuid){
                this.initiative += mech.initiative;
            } else {
                this.initiative += mech.initiative * this.leaderWeight;
            }
            
        }
        this .initiative = this.initiative / (mechs.size()-1+this.leaderWeight);
        this.buildSquad();
    }
    
    
    public void printSquad(){
        System.out.println("**************************");
        System.out.println("Squad name: " + this.name);
        System.out.println("Squad description: " + this.description);
        System.out.println("Squad faction: " + this.faction);
        System.out.println("Squad uuid: " + this.uuid);
        System.out.println("Squad leader weight: " + this.leaderWeight);
        System.out.println("Squad leader uuid: " + this.LeaderUuid);
        System.out.println("**************************");
        System.out.println("Squad mechs: ");
        for (Mech mech : mechs) {
            mech.printMech();
        }
        System.out.println("**************************");
        System.out.println("Squad stats: ");
        System.out.println("Squad strength: " + this.SquadStrength);
        System.out.println("Squad dexterity: " + this.SquadDexterity);
        System.out.println("Squad mech intelligence: " + this.SquadIntelligence);
        System.out.println("Squad mech will: " + this.SquadWill);
        System.out.println("Squad mech charisma: " + this.SquadCharisma);
        System.out.println("Squad mech perception: " + this.Squadperception);
        System.out.println("Squad mech speed: " + this.Squadspeed);
        System.out.println("Squad mech luck: " + this.Squadluck);
        System.out.println("Squad melee attack score: " + this.meleeAttackScore);
        System.out.println("Squad range attack score: " + this.rangeAttackScore);
        System.out.println("Squad evasion score: " + this.evasionScore);
        System.out.println("Squad init score: " + this.initScore);
        System.out.println("Squad presence score: " + this.presenceScore);
        System.out.println("**************************");
        
    }
    
    
    
    
}