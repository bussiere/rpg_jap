package main.java.trjengine.exception;

public class PositionNotFoundException extends Exception  {
    public PositionNotFoundException(String errorMessage) {  
        super(errorMessage);  
    }
    
}
