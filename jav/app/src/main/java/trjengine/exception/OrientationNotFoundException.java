package main.java.trjengine.exception;

public class OrientationNotFoundException extends Exception  {
    public OrientationNotFoundException(String errorMessage) {  
        super(errorMessage);  
    }
    
}
