package main.java.trjengine.dm.combat;

import java.util.ArrayList;
import java.util.List;

import main.java.trjengine.listenum.FactionEnum;
import main.java.trjengine.pawn.Pawn;

import java.util.Comparator;
import java.util.Collections;

public class Combat {

  static CombatRoll combatRoll = new CombatRoll();

  List<Pawn> team1 = new ArrayList<Pawn>();

  List<Pawn> team2 = new ArrayList<Pawn>();

  public class InitOrder{
    public int init;
    public FactionEnum faction;
    public Pawn pawn;

    public InitOrder(int init,FactionEnum faction,Pawn pawn){
      this.init = init;
      this.faction = faction;
      this.pawn = pawn;
    }
  }

  public static List<InitOrder> listInitOrder; 



  public void addPawn(Pawn pawn, FactionEnum faction) {
    pawn.initScore = 0;
    if (faction == FactionEnum.TEAM1) {
      team1.add(pawn);
    } else if (faction == FactionEnum.TEAM2) {
      team2.add(pawn);
    }
  }

  public static void rollIinit(List<Pawn> team1,List<Pawn> team2){
    listInitOrder = new ArrayList<InitOrder>();
    for (Pawn element : team1) {
      CombatRoll.rollInitiative(element);
      InitOrder pawnInit = new Combat().new InitOrder(element.initScore,FactionEnum.TEAM1,element);
      listInitOrder.add(pawnInit);
    }
    for (Pawn element : team2) {
      CombatRoll.rollInitiative(element);
      InitOrder pawnInit = new Combat().new InitOrder(element.initScore,FactionEnum.TEAM2,element);
      listInitOrder.add(pawnInit);
    }

    Collections.sort(listInitOrder, new Comparator<InitOrder>(){
     public int compare(InitOrder o1, InitOrder o2){
      return o2.init - o1.init;
    }
  });
  }

}
