package main.java.trjengine.dm.combat;

/*
Squad numbering :

[0,1,2]
[3,4,5]
[6,7,8]

*/

public class SquadCombat {

	public float[] modifDammageReceiver = new float[]{1,1,1,1,1,1,1,1,1};

	public float[] calcModifDammageReceivedFromAngle(int angleDammage) {
		this.modifDammageReceiver = new float[]{1,1,1,1,1,1,1,1,1};
		if (angleDammage == 135) {
			this.modifDammageReceiver[1] += 0.1;
			this.modifDammageReceiver[2] += 0.1;
			this.modifDammageReceiver[5] += 0.1;
		} else if (angleDammage == 90) {
			this.modifDammageReceiver[2] += 0.1;
			this.modifDammageReceiver[5] += 0.1;
			this.modifDammageReceiver[8] += 0.1;
		} else if (angleDammage == 45) {
			this.modifDammageReceiver[5] += 0.1;
			this.modifDammageReceiver[8] += 0.1;
			this.modifDammageReceiver[7] += 0.1;
		} else if (angleDammage == 0) {
			this.modifDammageReceiver[8] += 0.1;
			this.modifDammageReceiver[7] += 0.1;
			this.modifDammageReceiver[6] += 0.1;
		}  else if (angleDammage == 315) {
			this.modifDammageReceiver[7] += 0.1;
			this.modifDammageReceiver[6] += 0.1;
			this.modifDammageReceiver[3] += 0.1;
		} else if (angleDammage == 270) {
			this.modifDammageReceiver[6] += 0.1;
			this.modifDammageReceiver[3] += 0.1;
			this.modifDammageReceiver[0] += 0.1;
		} else if (angleDammage == 225) {
			this.modifDammageReceiver[3] += 0.1;
			this.modifDammageReceiver[0] += 0.1;
			this.modifDammageReceiver[1] += 0.1;
		}
		return this.modifDammageReceiver;

	}
	
}