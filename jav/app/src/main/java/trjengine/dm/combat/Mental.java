package main.java.trjengine.dm.combat;


import main.java.trjengine.dm.Dice;

import main.java.trjengine.pawn.Pawn;

import main.java.trjengine.dm.Dice.Result;

public class Mental{
  static Dice dice = new Dice();


public static int rollMoral(Pawn pawn) {
    int mentalResult = 0;
    Result result;
    result = dice.rollDiceWithBlue(3, 6);
    mentalResult = pawn.moralCurrent + (pawn.moralCurrent - result.sum);
    return mentalResult;
  }


}