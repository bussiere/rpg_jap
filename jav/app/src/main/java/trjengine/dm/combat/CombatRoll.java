package main.java.trjengine.dm.combat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import main.java.trjengine.dm.Dice;
import main.java.trjengine.dm.Dice.Result;
import main.java.trjengine.listenum.PawnTypeEnum;
import main.java.trjengine.mech.Mech;
import main.java.trjengine.pawn.Pawn;
import main.java.trjengine.squad.Squad;

public class CombatRoll {

  static Dice dice = new Dice();

  public class PawnInitComparator implements Comparator<Pawn> {

    @Override
    public int compare(Pawn pawn1, Pawn pawn2) {
      return compareInit(pawn1, pawn2);
    }
  }

  public static Pawn rollInitiative(Pawn pawn) {
    int initiative = 0;
    Result result;
    result = dice.rollDiceWithBlue(3, 6);
    initiative = pawn.initiative + (pawn.initiative - result.sum);
    pawn.initScore = initiative;
    return pawn;
  }

  public Mech rollInitiative(Mech pawn) {
    float initiative = 0;
    Result result;
    result = dice.rollDiceWithBlue(3, 6);
    initiative = pawn.initiative + (pawn.initiative - result.sum);
    pawn.initScore = initiative;
    return pawn;
  }

  public static int compareInit(Pawn pawn1, Pawn pawn2) {
    if (pawn1.initiative > pawn2.initiative) {
      return 1;
    } else if (pawn1.initiative < pawn2.initiative) {
      return -1;
    } else {
      return 0;
    }
  }

  public List<Mech> rollInitiative(List<Mech> pawns) {
    int i = 0;
    for (Mech pawn : pawns) {
      pawns.set(i, rollInitiative(pawn));
      i++;
    }
    Collections.sort(pawns, new PawnInitComparator());
    return pawns;
  }

  public List<Squad> rollInitiativeSquad(List<Squad> squads) {
    List<Squad> newSquads = new ArrayList<Squad>();
    for (Squad squad : squads) {
      List<Mech> pawns = squad.mechs;
      pawns = rollInitiative(pawns);
      for (Mech mech : pawns) {
        if (squad.mech1 != null && mech.uuid == squad.mech1.uuid) {
          squad.mech1.initiative = mech.initiative;
        } else if (squad.mech2 != null && mech.uuid == squad.mech2.uuid) {
          squad.mech2.initiative = mech.initiative;
        } else if (squad.mech3 != null && mech.uuid == squad.mech3.uuid) {
          squad.mech3.initiative = mech.initiative;
        } else if (squad.mech4 != null && mech.uuid == squad.mech4.uuid) {
          squad.mech4.initiative = mech.initiative;
        } else if (squad.mech5 != null && mech.uuid == squad.mech5.uuid) {
          squad.mech5.initiative = mech.initiative;
        } else if (squad.mech6 != null && mech.uuid == squad.mech6.uuid) {
          squad.mech6.initiative = mech.initiative;
        } else {
          System.out.println("Error in rollInitiativeSquad");
        }
        squad.recalcInitiative();
        newSquads.add(squad);
      }
    }
    Collections.sort(newSquads, new PawnInitComparator());
    return newSquads;
  }
}
