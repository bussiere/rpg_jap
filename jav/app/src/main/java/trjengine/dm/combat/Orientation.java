package main.java.trjengine.dm.combat;
import main.java.trjengine.listenum.OrientationEnum;
import main.java.trjengine.exception.OrientationNotFoundException;



public class Orientation {
	
	public static int calcAngle(int defender,int attacker) {
		int result = defender - attacker;
		if (result < 0){
			result = 360-result;
		}
		return result;
	}
	public static int getOrientation(OrientationEnum newOrientationEnum) throws OrientationNotFoundException {

         if (newOrientationEnum ==  OrientationEnum.NE) {
			return 45;
		} else if (newOrientationEnum ==  OrientationEnum.E) {
			return 90;
		}  else if (newOrientationEnum ==  OrientationEnum.SE) {
			return 135;
		} else if (newOrientationEnum ==  OrientationEnum.SW) {
			return 225;
		} else if (newOrientationEnum ==  OrientationEnum.W) {
			return 270;
		} else if (newOrientationEnum ==  OrientationEnum.NW) {
			return 315;
		}
		throw new OrientationNotFoundException("Empty String");
	}
	public static float angleDamageModifier(int angleDammage){
		if (angleDammage == 0) {
			return 1.45f;
		}else if (angleDammage == 135 || angleDammage == 225) {
			return 1.1f;
		} else if (angleDammage == 90 || angleDammage == 270) {
			return 1.21f;
		} else if (angleDammage == 45 || angleDammage == 315) {
			return 1.33f;
		}
		return 1;
	}

}