package main.java.trjengine.dm.combat;

import main.java.trjengine.map.Range;
import main.java.trjengine.map.Hex;
import main.java.trjengine.item.WeaponMecha;
import main.java.trjengine.item.WeaponMecha.WeaponMechaRangeCoordinate;
import main.java.trjengine.mech.Mech;
import main.java.trjengine.squad.Squad;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap; 
import main.java.trjengine.dm.combat.Attack;
import main.java.trjengine.listenum.DamageTypeEnum;
import main.java.trjengine.listenum.CrystalTypeEnum;
import main.java.trjengine.dm.combat.AttackBySquare;

/*
 * 
 * 
public enum CrystalTypeEnum {
    NEUTRAL,
    LIGHT,
    SHADOW,
    FLORA,
    WATER,
    FIRE,
    ICE,
    EARTH,
    VOLT,
    WIND,
    TOXIN,

    
}

 * 
 */
public class SquadAttack {

	public int minRangeAttacks;
	public int maxRangeAttacks;


	public HashMap<Integer, AttackBySquare> listAttackBySquare = new HashMap<Integer, AttackBySquare>();



	public void addAttack(Squad squad,Mech mech,WeaponMecha weaponMecha,int rangeDamage){
		addAttack(squad,mech,weaponMecha,rangeDamage,false);
	}
    // Todo calulate all atacks from a square
	public void addAttack(Squad squad,Mech mech,WeaponMecha weaponMecha,int rangeDamage,boolean critic) {
		for (WeaponMechaRangeCoordinate weaponMechaRangeCoordinate : weaponMecha.weaponMechasRangeCoordinate){
			AttackBySquare attackBySquare = listAttackBySquare.get((squad.posX+weaponMechaRangeCoordinate.plusPosX)*1000+(squad.posY+weaponMechaRangeCoordinate.plusPosY));
			if (attackBySquare == null){
				attackBySquare = new AttackBySquare();
			}
			attackBySquare.totalDamage += weaponMecha.dammageRange[rangeDamage];
			Attack attack = new Attack();
			// TODO
			attack.angleAttackFromOrigin = mech.orientationDegree;
			attack.attackerSquadUuid = squad.uuid;
			attack.attackerUuid = mech.uuid;
			attack.damageTypeEnum = weaponMecha.damageTypeEnum;
			attack.hexagonOriginUuid = squad.hexagonUuid;
			attack.weaponMechaRangeCoordinate = weaponMechaRangeCoordinate;
			attack.weaponMecha = weaponMecha;
			attack.crystalTypeEnum = mech.crystal.crystalTypeEnum;
			attackBySquare.listAttack.put(attackBySquare.nbAttack,attack);
			attackBySquare.nbAttack++;
			listAttackBySquare.put((squad.posX+weaponMechaRangeCoordinate.plusPosX)*1000+(squad.posY+weaponMechaRangeCoordinate.plusPosY),attackBySquare);
		}
	}


public void calculateAttackBySquare(Squad squad){
	calculateAttackBySquare(squad,10);
}
	public void calculateAttackBySquare(Squad squad,int rangeDamage){


		for (Mech mech : squad.mechs) {
			List<WeaponMecha> weaponMechas = new ArrayList<WeaponMecha>();
			weaponMechas.add(mech.rightHandWeaponMecha);
			weaponMechas.add(mech.leftHandWeaponMecha);
			weaponMechas.add(mech.backWeaponMecha);
			for(WeaponMecha weaponMecha : weaponMechas){
				if (weaponMecha != null){
					addAttack(squad,mech,weaponMecha,rangeDamage);
				}
			}
		}     
		

	}
}