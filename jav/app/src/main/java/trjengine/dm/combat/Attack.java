package main.java.trjengine.dm.combat;
import main.java.trjengine.listenum.OrientationEnum;
import main.java.trjengine.listenum.DamageTypeEnum;
import main.java.trjengine.item.WeaponMecha;
import main.java.trjengine.item.WeaponMecha.WeaponMechaRangeCoordinate;
import main.java.trjengine.listenum.CrystalTypeEnum;
import main.java.trjengine.tools.GenerateUuid;
/*
 * 


 * 
*/

public class Attack {

		public String uuid;
		public OrientationEnum orientationOrigin;
		public String attackerSquadUuid;
		public String attackerUuid;
		public String hexagonOriginUuid;
		public String weaponMechaUuid;
		public String factionUuid;
		public DamageTypeEnum damageTypeEnum;
		public CrystalTypeEnum crystalTypeEnum;
		public int angleAttackFromOrigin;
		public int angleAttackDefender;
		public WeaponMechaRangeCoordinate weaponMechaRangeCoordinate;
		public WeaponMecha weaponMecha;

		public Attack(){
			this.uuid = GenerateUuid.generateUuid();
		}


}