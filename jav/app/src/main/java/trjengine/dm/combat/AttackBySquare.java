package main.java.trjengine.dm.combat;

/*

    LIGHT,
    SHADOW,
    FLORA,
    WATER,
    FIRE,
    ICE,
    EARTH,
    VOLT,
    WIND,
    TOXIN,

*/

import main.java.trjengine.map.Hex;
import main.java.trjengine.dm.combat.Attack;
import main.java.trjengine.mech.Mech;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap; 
/* 
public enum CrystalTypeEnum {
    NEUTRAL,
    LIGHT,
    SHADOW,
    FLORA,
    WATER,
    FIRE,
    ICE,
    EARTH,
    VOLT,
    WIND,
    TOXIN,

    
}
*/
public class AttackBySquare {
		public int range;
		public int posX;
		public int posY;
		public int posXY;
		public int nbAttack;
		public float totalDamage;
		public float neutralKineticDamage;
		public float neutralLaserDamage;
		public float neutralMissileDamage;

		public float lightKineticDamage;
		public float lightLaserDamage;
		public float lightMissileDamage;

		public float shadowKineticDamage;
		public float shadowLaserDamage;
		public float shadowMissileDamage;

		public float floraKineticDamage;
		public float floraLaserDamage;
		public float floraMissileDamage;

		public float waterKineticDamage;
		public float waterLaserDamage;
		public float waterMissileDamage;

		public float fireKineticDamage;
		public float fireLaserDamage;
		public float fireMissileDamage;

		public float iceKineticDamage;
		public float iceLaserDamage;
		public float iceMissileDamage;

		public float earthKineticDamage;
		public float earthLaserDamage;
		public float earthMissileDamage;

		public float voltKineticDamage;
		public float voltLaserDamage;
		public float voltMissileDamage;

		public float windKineticDamage;
		public float windLaserDamage;
		public float windMissileDamage;

		public float toxinKineticDamage;
		public float toxinLaserDamage;
		public float toxinMissileDamage;


		public HashMap<Integer, Attack> listAttack;
		public HashMap<String, Hex> Hexes;
		public HashMap<String, Mech> Mechs;

		public AttackBySquare(){
			this.totalDamage = 0;
			this.neutralMissileDamage = 0;
			this.neutralLaserDamage = 0;
			this.neutralKineticDamage = 0;
			this.nbAttack =0;
		}


	}
