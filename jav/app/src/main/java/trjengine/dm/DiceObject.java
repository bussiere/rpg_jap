package main.java.trjengine.dm;

import main.java.trjengine.listenum.DiceTypeEnum;
import main.java.trjengine.tools.GenerateUuid;
public class DiceObject {

	public int value;
	public int nbFace;
	public String uuid;
	public DiceTypeEnum diceType;

	public DiceObject(){
		this.uuid = GenerateUuid.generateUuid();
	}



}