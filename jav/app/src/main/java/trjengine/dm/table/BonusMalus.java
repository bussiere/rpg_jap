package main.java.trjengine.dm.table;

import java.util.HashMap;
import java.util.Map;

public class BonusMalus {

  public static Map<Integer, Float> bonusTable = new HashMap<Integer, Float>();
  public static Map<Integer, Float> malusTable = new HashMap<Integer, Float>();
  public static Map<Integer, Integer> goldenBonus = new HashMap<Integer, Integer>();
  public static Map<Integer, Integer> goldenBonusDiv2 = new HashMap<Integer, Integer>();
  public static Map<Integer, Integer> goldenBonusDiv3 = new HashMap<Integer, Integer>();
  public static Map<Integer, Integer> goldenBonusDiv4 = new HashMap<Integer, Integer>();

  public BonusMalus() {
    float k;
    k = 1f;
    for (int i = 0; i < 100; i++) {
      bonusTable.put(i, 1 + (k / 10));
      malusTable.put(i, 1 - (k / 10));
      k = k * 1.2f;
    }
    int j = 0;
    k = 1f;
    for (int i = 1; i < 21; i++) {
      goldenBonus.put(i, j);
      k = k * 1.2f;
      j = (int) k;
    }
    j = 0;
    k = 1f;
    for (int i = 1; i < 21; i++) {
      goldenBonusDiv2.put(i, j);
      if (i % 2 == 0) k = k * 1.2f;
      j = (int) k;
    }
    j = 0;
    k = 1f;
    for (int i = 1; i < 21; i++) {
      goldenBonusDiv4.put(i, j);
      if (i % 4 == 0) k = k * 1.2f;
      j = (int) k;
    }
    j = 0;
    k = 1f;
    for (int i = 1; i < 21; i++) {
      goldenBonusDiv3.put(i, j);
      if (i % 3 == 0) k = k * 1.2f;
      j = (int) k;
    }
    System.out.println("bonusTable");
    System.out.println("bonusTable");
    System.out.println(bonusTable);
    System.out.println("malusTable");
    System.out.println("malusTable");
    System.out.println(malusTable);
    System.out.println("goldenBonus");
    System.out.println("goldenBonus");
    System.out.println(goldenBonus);
    System.out.println("goldenBonusDiv2");
    System.out.println("goldenBonusDiv2");
    System.out.println(goldenBonusDiv2);
    System.out.println("goldenBonusDiv3");
    System.out.println("goldenBonusDiv3");
    System.out.println(goldenBonusDiv3);
    System.out.println("goldenBonusDiv4");
    System.out.println("goldenBonusDiv4");
    System.out.println(goldenBonusDiv4);
  }
}
