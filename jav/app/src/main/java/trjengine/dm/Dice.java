package main.java.trjengine.dm;
import java.util.Random; 
import main.java.trjengine.listenum.DiceTypeEnum;
public class Dice {



public class Result {
        public DiceObject[] dicesObject;
        public int[] dices;
        public boolean success;
        public int sum;
        public int blueDice;
        public int redDice;
        public boolean criticalFailure;
        public boolean criticalSuccess;
        public Result() {
            this.success = false;
            this.criticalFailure = true;
            this.criticalSuccess = false;
        }
    }

	public Random r = new Random();

    public int averageRoll(int sides){
        int middle = (sides/2)+1;
        int quarter = (sides/4)+1;
        int rollQuarter = r.nextInt(quarter)+1;
        // binary roll 
        int roll = r.nextInt(2);
        if (roll == 0){
            return middle + rollQuarter;
        }else{
            return middle - rollQuarter;
        }
    }

    public int averageRoll(int dice,int sides){
        int result = 0;
        for (int i = 0; i < dice; i++){
            result += averageRoll(sides);
        }
        return result;
    }

	 public int roll(int dice, int sides) {
        Random r = new Random();
        int result = 0;
        for (int i = 0; i < dice; i++) {
            result += r.nextInt(sides) + 1;
        }
        return result;
    }
    
    public int getRandomNumberInRange(int min, int max) {
        
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
    
    public int[] getRandomNumbersInRange(int min, int max, int count) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        Random r = new Random();
        int[] result = new int[count];
        for (int i = 0; i < count; i++) {
            result[i] = r.nextInt((max - min) + 1) + min;
        }
        return result;
    }

    public Result rollDiceWithBlue(int poolDice,int nbSide){
        Result result = rollDice(poolDice,nbSide);
        result.dicesObject[poolDice-1].diceType = DiceTypeEnum.BLUE;
        return result;
    }

    public Result rollDiceWithBlueRed(int poolDice,int nbSide){
        Result result = rollDice(poolDice,nbSide);
        result.dicesObject[poolDice-1].diceType = DiceTypeEnum.BLUE;
        result.dicesObject[poolDice-2].diceType = DiceTypeEnum.RED;
        return result;
    }


    public Result rollDice(int poolDice,int nbSide){
        Result result = new Result();
        result.dicesObject = new DiceObject[poolDice];
        result.dices = getRandomNumbersInRange(1, nbSide,poolDice);
        result.sum = 0;
        for (int i =0; i < poolDice; i++){
            result.dicesObject[i] = new DiceObject();
            result.dicesObject[i].diceType = DiceTypeEnum.NORMAL;
            result.dicesObject[i].nbFace = nbSide;
            result.sum += result.dices[i];
        }
        
        return result;
        
    }

}