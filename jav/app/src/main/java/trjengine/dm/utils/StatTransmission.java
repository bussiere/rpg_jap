package main.java.trjengine.dm.utils;

public class StatTransmission {

  public int strength = 0;
  public int dexterity = 0;
  public int constitution = 0;
  public int intelligence = 0;
  public int will = 0;
  public int charisma = 0;

  public int lifePool = 0;
  public int mindPool = 0;

  public int accuracy = 0;

  public StatTransmission(
    int strength,
    int dexterity,
    int constitution,
    int intelligence,
    int will,
    int charisma
  ) {
    this.strength = strength;
    this.dexterity = dexterity;
    this.constitution = constitution;
    this.intelligence = intelligence;
    this.will = will;
    this.charisma = charisma;
  }
}
