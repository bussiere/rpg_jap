package main.java.trjengine.map;
public class Range {


    public int xPlus;
    public int yPlus;
    public int zPlus;
    
    public int xCalculated;
    public int yCalculated;
    public int zCalculated;

    public int xyCalculated;

    public void xyCalculated(){
        this.xyCalculated = this.xCalculated *1000+this.yCalculated;
    }

    public void resetCalculated(){
        this.xCalculated = 0;
        this.yCalculated = 0;
        this.zCalculated = 0;
        this.xyCalculated = 0;
    }

    public void initialize(int x,int y,int z){
        this.xPlus = x;
        this.yPlus = y;
        this.zPlus = z;

    }
    public void rangeCoordinate(int x,int y,int z) {
        initialize(x,y,z);

    }
    public void rangeCoordinate(int x,int y) {
        initialize(x,y,0);

    }
}


