package main.java.trjengine.map;
import main.java.trjengine.tools.GenerateUuid;
public class Hex {

    public int posX;
    public int posY;
    public int posZ;
    public int height;
    public int width;
    public int depth;
    public int type;
    public int terrain;
    public int terrain_type;
    public int terrain_subtype;
    public String uuidOccupant;
    public String uuid;
    public boolean isOccupied;
    public String faction;
    public int posXY;


    public Hex() {

        this.uuid = GenerateUuid.generateUuid();
    }

    public void updateXY(){
        this.posXY = this.posX*1000+this.posY;
    }


    

    
}
