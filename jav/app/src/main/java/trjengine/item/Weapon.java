package main.java.trjengine.item;
import main.java.trjengine.item.Item;
import main.java.trjengine.listenum.CrystalTypeEnum;
import main.java.trjengine.listenum.WeaponCategoryEnum;
import java.util.List;
import java.util.ArrayList;
public class Weapon extends Item {


    public int damageMin;
    public int damageMax;
    public  WeaponCategoryEnum  weaponCategoryEnum;
    public int bonusToHit;
    public int level;

    public CrystalTypeEnum crystalTypeEnumDamage1;
    public List<Integer> minDamageArrayCrystalTypeEnumDamage1 = new ArrayList<Integer>();
    public List<Integer> maxDamageArrayCrystalTypeEnumDamage1 = new ArrayList<Integer>();

    public CrystalTypeEnum crystalTypeEnumDamage2;
    public List<Integer> minDamageArrayCrystalTypeEnumDamage2 = new ArrayList<Integer>();
    public List<Integer> maxDamageArrayCrystalTypeEnumDamage2 = new ArrayList<Integer>();

    public CrystalTypeEnum crystalTypeEnumDamage3;
    public List<Integer> minDamageArrayCrystalTypeEnumDamage3 = new ArrayList<Integer>();

    public List<Integer> maxDamageArrayCrystalTypeEnumDamage3 = new ArrayList<Integer>();

    // ArrayList for min,max damage by range of bonus on the roll 

    public List<Integer> minDamageArray = new ArrayList<Integer>();
    public List<Integer> maxDamageArray = new ArrayList<Integer>();


    // return an array of two int with min and max damage
    public int[] getDammageMinArray(int marge){
        int[] array = new int[2];
        array[0] = this.minDamageArray.get(marge);
        array[1] = this.maxDamageArray.get(marge);
        return array;
    }

    public void generateMinMaxDamageArray(){
        double i = 0;

        while (i < 30) {
            this.minDamageArray.add((int)((double)this.damageMin*(i*1.2)));
            this.maxDamageArray.add((int)((double)this.damageMax*(i*1.2)));
            i++;
        }

    }




    public void constructWeapon(){
        this.damageMin = 10;
        this.damageMax = 10;
        this.bonusToHit = 0;
        this.weaponCategoryEnum = WeaponCategoryEnum.AXE;
        generateMinMaxDamageArray();


    }
    public Weapon(){
        constructWeapon();

    } 

    public Weapon(WeaponCategoryEnum weaponCategoryEnum){
         constructWeapon();
        this.weaponCategoryEnum = weaponCategoryEnum;
    }


}
