package main.java.trjengine.item;
import main.java.trjengine.effect.Effect;
import main.java.trjengine.listenum.ItemTypeEnum;
import main.java.trjengine.status.Status;
import main.java.trjengine.pawn.Pawn;
import java.util.ArrayList;
import java.util.List;
import main.java.trjengine.tools.GenerateUuid;

public class Item {

	public String name;
	public String description;
	public ItemTypeEnum type;
	public String uuid;

	public int weight;

	public int price;

	public int strengthBonus;

	public int dexterityBonus;

	public int constitutionBonus;

	public int intelligenceBonus;

	public int wisdomBonus;

	public int charismaBonus;

	public int perceptionBonus;

	public Effect effectOnAttack(){
		return null;
	}

	public Effect effectOnDefense(){
		return null;
	}

	public List<Effect> attackBuff = new ArrayList<>();

	public List<Effect> attackDebuff = new ArrayList<>();

	public List<Effect> defenseBuff = new ArrayList<>();

	public List<Effect> defenseDebuff = new ArrayList<>();

	public List<Effect> caracBuff = new ArrayList<>();


	public List<Effect> caracBonus = new ArrayList<>();

	public List<Effect>caracMalus =new ArrayList<>();

	public List<Status>statusMalus =new ArrayList<>();

	public List<Status>statusBonus =new ArrayList<>();

	public Item(String name, String description, ItemTypeEnum type) {
		this.name = name;
		this.description = description;
		this.type = type;
		this.uuid = GenerateUuid.generateUuid();
	}
	public Item() {
		this.uuid = GenerateUuid.generateUuid();
	}

	public Pawn equipItem(Pawn pawn){
		// get item in position
		return pawn;
	}

	public Pawn unequipItem(Pawn pawn){
		// get item in position
		return pawn;
	}

	public Pawn attackAffectPawn(Pawn pawn){
		// add effect to pawn enemy
		return pawn;
	}


		

}