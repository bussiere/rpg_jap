package main.java.trjengine.item;
import main.java.trjengine.listenum.DurabilityEnum;
import main.java.trjengine.listenum.ItemTypeEnum;
import main.java.trjengine.listenum.CrystalTypeEnum;
public class Crystal extends Item {
    public DurabilityEnum durabilityEnum;
    public CrystalTypeEnum crystalTypeEnum;
    public Crystal() {
        super();
        this.name = "Crystal";
        this.description = "A crystal that can be used to summon a rosewind.";
        this.type = ItemTypeEnum.CRYSTAL;
        this.crystalTypeEnum = CrystalTypeEnum.NEUTRAL;
    }
}
    