package main.java.trjengine.ship;
import java.util.ArrayList;
import java.util.List;
import main.java.trjengine.pilot.Pilot;
import main.java.trjengine.pawn.Pawn;
import main.java.trjengine.listenum.PawnTypeEnum;

public class Ship extends Pawn {

		public List<Pilot> pilots = new ArrayList<Pilot>();

		public Ship(String name) {
			super(name);
			this.pawnTypeEnum = PawnTypeEnum.SHIP;
		}

}