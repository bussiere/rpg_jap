package main.java.trjengine.pilot;

import main.java.trjengine.pj.Pj;

public class Pilot extends Pj {
	


	public Pilot(String name) {
		super(name);
		init();
	}
	public Pilot() {
		super();
		init();

	}

	public void resetStat() {
		this.strengthCurrent = this.strength;

	}


	public void escalate(){
		resetStat();
		escalationDice.escalate();
		float bonus = escalationDice.getBonus();
		this.strengthCurrent+= bonus;
		this.dexterityCurrent+= bonus;
		this.constitutionCurrent+= bonus;
		this.intelligenceCurrent+= bonus;
		this.willCurrent+= bonus;
		this.charismaCurrent+= bonus;
		this.perceptionCurrent+= bonus;
		this.speedCurrent+= bonus;
		this.luckCurrent+= bonus;
		this.moralCurrent+= bonus;


	}

	public void printPilot(){
		System.out.println("====================");
		System.out.println("pilot name: " + this.name);
		System.out.println("pilot uuid: " + this.uuid);
		System.out.println("pilot strength: " + this.strength);
		System.out.println("pilot dexterity: " + this.dexterity);
		System.out.println("pilot constitution: " + this.constitution);
		System.out.println("pilot intelligence: " + this.intelligence);
		System.out.println("pilot will: " + this.will);
		System.out.println("pilot charisma: " + this.charisma);
		System.out.println("pilot perception: " + this.perception);
		System.out.println("pilot speed: " + this.speed);
		System.out.println("pilot luck: " + this.luck);
		System.out.println("pilot moral: " + this.moral);
		System.out.println("====================");


	}

}