package main.java.trjengine.pawn;

import main.java.trjengine.listenum.PawnTypeEnum;
import main.java.trjengine.listenum.EscalationDiceEnum;
import main.java.trjengine.dm.combat.Orientation;
import main.java.trjengine.listenum.OrientationEnum;
import main.java.trjengine.exception.OrientationNotFoundException;
import main.java.trjengine.tools.GenerateUuid;
public class PawnMech  extends Pawn{
    

    public PawnTypeEnum pawnTypeEnum;
    public String name;
    public String uuid;
    public String hexagonUuid;

    public int posX;
    public int posY;
    public int posXY;

    public float meleeAttackScore;
	public float rangeAttackScore;
	public float evasionScore;
	public float initScore;
	public float criticalScore;
	public float presenceScore;

    public float initiative;
    
    public EscalationDiceEnum escalationDiceEnum;

    public OrientationEnum orientationEnum;
    public int orientationDegree;

    public void positionUpdate(int x,int y){
        this.posX = x;
        this.posY = y;
        this.posXY = x*1000+y;
    }

    public void changeOrientation(OrientationEnum newOrientationEnum) throws OrientationNotFoundException{
        this.orientationEnum = newOrientationEnum;
        this.orientationDegree =  Orientation.getOrientation(this.orientationEnum);
    }

    public void initialize(){
        this.orientationDegree = 0;
        this.orientationEnum = OrientationEnum.NE;
        this.uuid = GenerateUuid.generateUuid();
    }
    public PawnMech(){
        initialize();
    }

    public PawnMech(String name) {
        this.name = name;
        initialize();
        
    }
}
