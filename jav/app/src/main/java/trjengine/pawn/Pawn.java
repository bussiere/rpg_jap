package main.java.trjengine.pawn;


import java.util.ArrayList;
import java.util.List;


import main.java.trjengine.classpawn.ClassPawn;
import main.java.trjengine.classpawn.Thief;
import main.java.trjengine.classpawn.Warrior;
import main.java.trjengine.item.Weapon;
import main.java.trjengine.listenum.PawnTypeEnum;
import main.java.trjengine.inventory.InventoryPawn;
import main.java.trjengine.listenum.InventoryPositionEnum;

import main.java.trjengine.effect.Effect;

import main.java.trjengine.status.Status;
import main.java.trjengine.tools.GenerateUuid;


public class Pawn {
  
  public float goldenNumber = 1.2f;
  
  public PawnTypeEnum pawnTypeEnum;
  public String name;
  public String uuid;
  
  public ClassPawn primaryClass;
  public ClassPawn secondaryClass;

  public InventoryPawn inventoryPawn;
  
  public int strength;
  public int dexterity;
  public int constitution;
  public int intelligence;
  public int will;
  public int charisma;

  public int perception;
  
  public int evasion;
  public int healthPoint;
  
  public int strengthWithEffects;
  public int dexterityWithEffects;
  public int constitutionWithEffects;
  public int intelligenceWithEffects;
  public int willWithEffects;
  public int charismaWithEffects;
  
  public int strengthWithItems;
  public int dexterityWithItems;
  public int constitutionWithItems;
  public int intelligenceWithItems;
  public int willWithItems;
  public int charismaWithItems;

  public int perceptionWithItems;
  
  public int strengthCurrent;
  public int dexterityCurrent;
  public int constitutionCurrent;
  public int intelligenceCurrent;
  public int willCurrent;
  public int charismaCurrent;

  public int perceptionCurrent;
  
  public int evasionCurrent;
  public int healthPointCurrent;
  
  public int strengthBurned;
  public int dexterityBurned;
  public int constitutionBurned;
  public int intelligenceBurned;
  public int willBurned;
  public int charismaBurned;
  
  public int resistance;
  public int might;
  
  public int resistanceCurrent;
  public int mightCurrent;
  
  public int initiative;
  public int initiativeCurrent;
  
  public int initScore;
  
  public int strikes;
  
  public int lifePool;
  
  public int mindPool;
  
  public int moral;
  public int moralCurrent;
  
  public int luck;
  public int luckCurrent;
  

  
  public int speed;
  public int speedCurrent;
  
  public int acSlash;
  public int acBlunt;
  public int acPierce;
  
  public int baseAccuracy;
  public int magicAccuracy;
  public int distantAccuracy;


    public int damageSlash;

    public int damageSlashMin;

    public int damageSlashMax;
    public int damageBlunt;

    public int damageBluntMin;

    public int damageBluntMax;
    public int damagePierce;

    public int damagePierceMin;

    public int damagePierceMax;

    public int baseDamage;

    public int baseDamageMin;

    public int baseDamageMax;

public List<Effect> attackBuff = new ArrayList<>();

public List<Effect> attackDebuff = new ArrayList<>();

public List<Effect> defenseBuff = new ArrayList<>();

public List<Effect> defenseDebuff = new ArrayList<>();

public List<Effect> caracBuff = new ArrayList<>();


public List<Effect> caracBonus = new ArrayList<>();

public List<Effect>caracMalus =new ArrayList<>();

public List<Status>statusMalus =new ArrayList<>();

public List<Status>statusBonus =new ArrayList<>();
  
  public Pawn() {
    this.strengthBurned = 0;
    this.dexterityBurned = 0;
    this.constitutionBurned = 0;
    this.intelligenceBurned = 0;
    this.willBurned = 0;
    this.charismaBurned = 0;
    this.inventoryPawn = new InventoryPawn();
    this.uuid = GenerateUuid.generateUuid();
    
    
  }
  
  public Pawn(String name) {
    this();
    this.name = name;
    this.primaryClass = new Warrior();
    this.secondaryClass = new Thief();
  }
  
  public void UpdateCurrent() {}
  
  public void printClass() {
    System.out.println("");
    System.out.println("Primary Class : " + this.primaryClass.name);
    // print description
    System.out.println("");
    System.out.println("Description : " + this.primaryClass.description);
    System.out.println("");
    System.out.println("Secondary Class : " + this.secondaryClass.name);
    // print description
    System.out.println("");
    System.out.println("Description : " + this.secondaryClass.description);
  }
  
  public void printCarac() {
    System.out.println("");
    System.out.println("strength : " + this.strengthCurrent);
    System.out.println("");
    System.out.println("dexterity : " + this.dexterityCurrent);
    System.out.println("");
    System.out.println("constitution : " + this.constitutionCurrent);
    System.out.println("");
    System.out.println("intelligence : " + this.intelligenceCurrent);
    System.out.println("");
    System.out.println("will : " + this.willCurrent);
    System.out.println("");
    System.out.println("charisma : " + this.charismaCurrent);
  }
  
  public void printPostCarac() {
    System.out.println("");
    System.out.println(
    "healthPoint : " + this.healthPointCurrent + "/" + this.healthPoint
    );
    System.out.println("");
    System.out.println("evasion : " + this.evasionCurrent + "/" + this.evasion);
    System.out.println("");
    System.out.println("moral : " + this.moralCurrent + "/" + this.moral);
    // print initiative
    System.out.println("");
    System.out.println("initiative : " + this.initiativeCurrent);
    System.out.println("");
    System.out.println("initScore : " + this.initScore);
    System.out.println("");
    System.out.println("strikes : " + this.strikes);
    System.out.println("");
    System.out.println("lifePool : " + this.lifePool);
    System.out.println("");
    System.out.println("mindPool : " + this.mindPool);
    System.out.println("");
    System.out.println("might" + this.mightCurrent + "/" + this.might);
  }
  
  public int calculateHealthPoint() {
    return (
    this.primaryClass.calculateHealthPoint(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) +
    (
    (
    (int) (
    this.secondaryClass.calculateHealthPoint(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) /
    this.goldenNumber
    )
    )
    )
    );
  }
  
  public int calculateEvasion() {
    System.out.println("");
    System.out.println("intelligence");
    System.out.println("");
    System.out.println("this.intelligence");
    System.out.println(this.intelligence);
    System.out.println("");
    System.out.println("dexterity");
    System.out.println("");
    System.out.println("this.dexterity");
    System.out.println(this.dexterity);
    System.out.println("");
    System.out.println("intelligenceCurrent");
    System.out.println("");
    System.out.println("this.intelligenceCurrent");
    System.out.println(this.intelligenceCurrent);
    System.out.println("");
    System.out.println("dexterityCurrent");
    System.out.println("");
    System.out.println("this.dexterityCurrent");
    System.out.println(this.dexterityCurrent);
    return (
    this.primaryClass.calculateEvasion(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) +
    (int) (
    this.secondaryClass.calculateEvasion(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) - this.secondaryClass.caracUsedForEvasion(strength, dexterity, constitution, intelligence, will, charisma)
    )
    );
  }
  
  public int calculateMight() {
    return (
    (
    this.primaryClass.calculateMight(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) +
    (int) (
    this.secondaryClass.calculateMight(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    )- this.secondaryClass.caracUsedForMight(strength, dexterity, constitution, intelligence, will, charisma)
    )
    )
    );
  }

  public void inventoryEquipRightHand(Weapon weapon) {
    this.inventoryPawn.mapPositionItem.put(InventoryPositionEnum.RIGHT_HAND, weapon);
  }

  public void inventoryEquipLeftHand(Weapon weapon) {
    this.inventoryPawn.mapPositionItem.put(InventoryPositionEnum.LEFT_HAND, weapon);
  }

  public void inventoryEquipBothHands(Weapon weapon) {
    this.inventoryPawn.mapPositionItem.put(InventoryPositionEnum.BOTH_HANDS, weapon);
  }

  public void inventoryEquipHead(Weapon weapon) {
    this.inventoryPawn.mapPositionItem.put(InventoryPositionEnum.HEAD, weapon);
  }

    public void inventoryEquipCollar(Weapon weapon) {
        this.inventoryPawn.mapPositionItem.put(InventoryPositionEnum.COLLAR, weapon);
    }

    public void inventoryEquipTorso(Weapon weapon) {
        this.inventoryPawn.mapPositionItem.put(InventoryPositionEnum.TORSO, weapon);
    }

    public void inventoryEquipRightWrist(Weapon weapon) {
        this.inventoryPawn.mapPositionItem.put(InventoryPositionEnum.RIGHT_WRIST, weapon);
    }

    public void inventoryEquipLeftWrist(Weapon weapon) {
        this.inventoryPawn.mapPositionItem.put(InventoryPositionEnum.LEFT_WRIST, weapon);
    }

    public void inventoryEquipRightFinger(Weapon weapon) {
        this.inventoryPawn.mapPositionItem.put(InventoryPositionEnum.RIGHT_FINGER, weapon);
    }

    public void inventoryEquipLeftFinger(Weapon weapon) {
        this.inventoryPawn.mapPositionItem.put(InventoryPositionEnum.LEFT_FINGER, weapon);
    }

    public void inventoryEquipBoots(Weapon weapon) {
        this.inventoryPawn.mapPositionItem.put(InventoryPositionEnum.BOOTS, weapon);
    }

    public void inventoryEquipCape(Weapon weapon) {
        this.inventoryPawn.mapPositionItem.put(InventoryPositionEnum.CAPE, weapon);
    }

  
  public int calculateResistance() {
    return (
    (
    this.primaryClass.calculateResistance(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) +
    (int) (
    this.secondaryClass.calculateResistance(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) /
    this.goldenNumber
    )
    )
    );
  }
  
  public int calculateInit() {
    return (
    (
    this.primaryClass.calculateInitScore(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) +
    (int) (
    this.secondaryClass.calculateInitScore(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) - this.secondaryClass.caracUsedForInit(strength, dexterity, constitution, intelligence, will, charisma)
    )
    )
    );
  }
  
  public int calculateMoral() {
    return (
    (
    this.primaryClass.calculateMoral(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) +
    (int) (
    this.secondaryClass.calculateMoral(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) /
    this.goldenNumber
    )
    )
    );
  }
  
  public int calculateLifePool() {
    return (this.healthPoint+
    (
    this.primaryClass.calculateLifePool(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) +
    (int) (
    this.secondaryClass.calculateLifePool(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) /
    this.goldenNumber
    )
    )
    );
  }
  
  public int calculateMindPool() {
    return (
    (
    this.primaryClass.calculateMindPool(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) +
    (int) (
    this.secondaryClass.calculateMindPool(
    this.strengthCurrent,
    this.dexterityCurrent,
    this.constitutionCurrent,
    this.intelligenceCurrent,
    this.willCurrent,
    this.charismaCurrent
    ) /
    this.goldenNumber
    )
    )
    );
  }
  
  public void updateCurrent() {
    this.strengthCurrent = this.strength;
    this.dexterityCurrent = this.dexterity;
    this.constitutionCurrent = this.constitution;
    this.intelligenceCurrent = this.intelligence;
    this.willCurrent = this.will;
    this.charismaCurrent = this.charisma;
    this.healthPointCurrent = this.healthPoint;
    this.evasionCurrent = this.evasion;
    this.mightCurrent = this.might;
    this.resistanceCurrent = this.resistance;
    this.moralCurrent = this.moral;
    this.initiativeCurrent = this.initiative;
  }
  
  public void calculatePool() {
    this.mindPool = this.calculateMindPool();
    this.lifePool = this.calculateLifePool();
  }
  
  public void generatePostCarac() {
    this.healthPoint = this.calculateHealthPoint();
    this.evasion = this.calculateEvasion();
    this.might = this.calculateMight();
    this.resistance = this.calculateResistance();
    this.moral = this.calculateMoral();
    this.initiative = this.calculateInit();
    this.updateCurrent();
    this.calculatePool();
    
  }
}