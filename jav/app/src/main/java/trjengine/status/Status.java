package main.java.trjengine.status;

import main.java.trjengine.pawn.Pawn;
import main.java.trjengine.listenum.StatusPositiveEnum;
import main.java.trjengine.listenum.StatusNegativeEnum;

public class Status {

    public int id;
    public String name;
    public String description;
    public int level;

    public boolean random;

    public int probability;

    public int strongnessEffect;

    public int damage;

    public int heal;

    public int damageMax;

    public int damageMin;


    public int range;
    public int area;
    public int duration;
    public int cooldown;
    public int castTime;

    public String log;

    public int nbTurnDuration;

    // add enum StatusTypeEnum

    public StatusPositiveEnum statusPositiveEnum;

    public StatusNegativeEnum statusNegativeEnum;

	
}
