package main.java.trjengine.status;

import main.java.trjengine.pawn.Pawn;

public class StatusManager {

    public Pawn StatusMalusApplyPawn(Pawn pawn,Status status){
        if(status.nbTurnDuration <= 0){
            pawn.statusMalus.remove(status);
        }
        else{
            // check if status is already applied
            if(!pawn.statusMalus.contains(status)){
                pawn.statusMalus.add(status);
            }
        }

        status.nbTurnDuration--;
        return pawn;
    }
}
